package goose.plugin.models;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;

public interface IRSItem extends IAdaptable {
  String getDescription();

  Integer getLocation();

  String getType();

  String getResourceName();
  
  String getResourcePath();
  
  boolean isSuggestionFor(IResource obj);

  static IRSItem[] NONE = new IRSItem[] {};

}
