package goose.plugin.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IResource;

public class RSManager {
  private static RSManager manager;
  private Map<IResource, Collection<IRSItem>> refactoringSuggestions;

  private RSManager() {
  }

  public static RSManager getManager() {
    if (manager == null)
      manager = new RSManager();
    return manager;
  }

  public IRSItem[] getRefactoringSuggestions() {
    if (refactoringSuggestions == null)
      refactoringSuggestions = new HashMap<>();
    Collection<IRSItem> allItems = new ArrayList<>();
    for (Collection<IRSItem> c : refactoringSuggestions.values()) {
      allItems.addAll(c);
    }
    return allItems.toArray(new IRSItem[allItems.size()]);
  }

  public void addRefactoringSuggestions(IResource r, Collection<IRSItem> items) {
    if (refactoringSuggestions == null)
      refactoringSuggestions = new HashMap<>();
    Collection<IRSItem> removed = refactoringSuggestions.get(r);
    if (removed == null)
      removed = new ArrayList<>();
    refactoringSuggestions.put(r, items);
    fireRefactoringSuggestionsChanged(removed, items);
  }

  private List<RSManagerListener> listeners = new ArrayList<RSManagerListener>();

  public void addManagerListener(RSManagerListener listener) {
    if (!listeners.contains(listener))
      listeners.add(listener);
  }

  public void removeManagerListener(RSManagerListener listener) {
    listeners.remove(listener);
  }

  private void fireRefactoringSuggestionsChanged(
      Collection<IRSItem> itemsRemoved, Collection<IRSItem> itemsAdded) {
    RSManagerEvent event = new RSManagerEvent(this, itemsRemoved, itemsAdded);
    for (RSManagerListener l : listeners)
      l.refactoringSuggestionsChanged(event);
  }

}
