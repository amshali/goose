package goose.plugin;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

public class GooseActivator extends AbstractUIPlugin {

  // The plug-in ID
  public static final String PLUGIN_ID = "Goose";

  // The shared instance
  private static GooseActivator plugin;

  @Override
  public void start(BundleContext context) throws Exception {
    super.start(context);
    plugin = this;
  }

  /**
   * Returns the shared instance
   */
  public static GooseActivator getDefault() {
    return plugin;
  }

  @Override
  public void stop(BundleContext context) throws Exception {
    // TODO Auto-generated method stub
  }

  /**
   * Returns an image descriptor for the image file at the given plug-in
   * relative path
   * 
   * @param path
   *          the path
   * @return the image descriptor
   */
  public static ImageDescriptor getImageDescriptor(String path) {
    return imageDescriptorFromPlugin(PLUGIN_ID, path);
  }

}
