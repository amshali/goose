package goose.refactorings.cce;

import goose.suggestion.CCEOrRefactoringSuggestion;
import goose.visitors.Context;
import goose.visitors.DefaultTreeVisitor;

import java.util.ArrayList;
import java.util.List;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.Tree;

public class ConsolidateOrStateMachine extends DefaultTreeVisitor {

  private enum State {
    Start, If1, If2Plus, Analyze
  }

  private int MIN_IFS = 2;

  public ConsolidateOrStateMachine(int mIN_IFS) {
    super();
    MIN_IFS = mIN_IFS;
  }

  private State state = State.Start;

  private List<IfTree> ifs = new ArrayList<IfTree>();

  private void gotoStart() {
    state = State.Start;
    ifs.clear();
  }

  private void gotoAnalyze(Context p) {
    state = State.Analyze;
    int startIndex = 0;
    IfTree prev = ifs.get(startIndex);
    int i = 1;
    for (; i < ifs.size(); i++) {
      IfTree current = ifs.get(i);
      if (!prev.getThenStatement().toString()
          .equals(current.getThenStatement().toString())) {
        if (i - startIndex >= MIN_IFS) {
          suggestions.add(new CCEOrRefactoringSuggestion(p, prev, ifs
              .get(i - 1)));
        }
        startIndex = i;
        prev = current;
      }
    }
    if (i - startIndex >= MIN_IFS) {
      suggestions.add(new CCEOrRefactoringSuggestion(p, prev, ifs.get(i - 1)));
    }
    gotoStart();
  }

  private void gotoIf1() {
    state = State.If1;
  }

  private void gotoIf2Plus() {
    state = State.If2Plus;
  }

  private Boolean changeNonIf(Context p) {
    switch (state) {
      case Start:
        break;
      case If1:
        gotoStart();
        break;
      case If2Plus:
        gotoAnalyze(p);
        break;
      default:
        break;
    }
    return true;
  }

  private Boolean changeIf(IfTree node, Context p) {
    ifs.add(node);
    switch (state) {
      case Start:
        gotoIf1();
        break;
      case If1:
        gotoIf2Plus();
        break;
      case If2Plus:
        break;
      default:
        break;
    }
    return true;
  }

  @Override
  protected Boolean defaultAction(Tree t, Context p) {
    return changeNonIf(p);
  }

  @Override
  public Boolean visitBlock(BlockTree node, Context p) {
    for (StatementTree st : node.getStatements()) {
      st.accept(this, p);
    }
    changeNonIf(p);
    return true;
  }

  @Override
  public Boolean visitIf(IfTree node, Context p) {
    if (!hasElse(node) && endsWithReturn(node)) {
      changeIf(node, p);
    } else {
      changeNonIf(p);
    }
    return true;
  }

  private Boolean hasElse(IfTree node) {
    return node.getElseStatement() != null;
  }

  private Boolean endsWithReturn(IfTree node) {
    EndsWithReturnVisitor endsWithReturnVisitor = new EndsWithReturnVisitor();
    return node.getThenStatement().accept(endsWithReturnVisitor, null);
  }

}
