package cdcf;

/**
 * CDCF false positive tests.
 * 
 * @author QualityCan
 */
public class FP {
  public boolean b;
  public int i;
  public double d;
  
  /**
   * Side effects.
   */
  public void test1() {
    if (sideEffect()) {
      this.i++;
      System.out.println("M1");
      System.out.println("E");
    } else {
      this.i++;
      System.out.println("M2");
      System.out.println("E");
    }
  }

  private boolean sideEffect() {
    return this.i > 0;
  }
}
