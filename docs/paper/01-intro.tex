\section{Introduction}
\label{sec:introduction}
Refactoring is a set of techniques and practices for altering software 
while maintaining its functionality in order to improve its 
quality~\cite{Fowler1999}. In this context, software quality translates 
to the amount of effort that is needed to 
maintain~\cite{Griswold:1992:PRA:144627}
it. Software is not a static entity; it changes over time for different 
reasons such as adding new features and fixing bugs. Refactoring makes 
these activities easier by eliminating what are known as \textit{code 
smells}~\cite{Fowler1999}.


A code smell is a bad programming practice that can cause the code to be 
unreadable or prone to bugs. Examples of code smells include long methods, 
complex conditional expressions, and duplicated code. Over the course of 
the software development process, continued use of these bad practices 
causes the software to become difficult to maintain and change. Refactoring 
transforms code smells in ways that eliminate these problems.


The refactoring process has three parts: 
\begin{enumerate}
\item Identifying what refactorings to apply and where to apply them
\item Applying the desired refactorings
\item Testing to verify the refactored code is functionally the same
\end{enumerate}
The first step in refactoring is to identify locations in code where 
it can be applied. In order to spot suitable locations it is necessary 
to find examples of refactoring patterns. A refactoring pattern is a 
formally-defined structure that can be matched with code so that the 
code can be rewritten. For instance, the \textit{encapsulate field} pattern 
describes a situation where a public field can be restructured into a 
private variable with accessor methods. Fowler~\cite{Fowler1999} gives 
over 60 patterns in his book, many of which are widely known and used 
in industry.


After knowing what kind of refactorings apply and where to apply them, 
the process of changing the structure of the code is mostly mechanical. 
IDEs such as Eclipse, IntelliJ IDEA, and NetBeans that will perform 
many refactorings automatically. Refactorings performed this way are 
provably correct such that the third step, testing, is not necessary. 
However, refactorings applied by hand may need to be verified, depending 
on the implementer's confidence.


Despite all the work that has been done in this area, we think that there 
are still opportunities left to exploit. In our work we are interested 
in the first part of the refactoring process: identifying the locations 
where one or more refactorings can be applied. In particular, our work 
focusing on identifying refactoring patterns relating to \lstinline{if-else} 
constructs. To motivate this limited scope we conducted an experiment to 
measure the amount of code which is related to \lstinline{if-else} 
constructs. We took two measurements over three open-source codebases: 
\textit{HSQLDB}, \textit{Apache Ant} and \textit{Findbugs}. These codebases 
are also used in Section~\ref{sec:evaluation} of this paper. The first 
measurement is lines of code inside \lstinline{if-else} 
constructs divided by lines of code inside methods, and the second is the 
average number of \lstinline{if} conditions per file. 
Table~\ref{tab:if-else-coverage-average} gives the two measurements for each 
codebase.


\begin{table}[t]
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline  & FindBugs & Ant & HSQLDB \\
\hline if-else coverage & 35.99\% & 41.76\% & 32.72\% \\
\hline if-conditions average & 9.84 & 10.21 & 24.44 \\
\hline
\end{tabular}
\end{center}
\caption{Coverage percentage of code by if-else statements and average number 
of if-conditions per each file on three Java open-source projects}
\label{tab:if-else-coverage-average}
\end{table}


The ratio of conditionally-executed code to method lines ranges from 32 to 41 
percent. In addition, on average there are 12.98 \lstinline{if} statements 
per Java file. Each file on average has 330 lines of code. This means that 
every 25 lines of code you can expect to see an if-condition. 
We believe that this data demonstrates that \lstinline{if-else} constructs are 
common enough that fixing problems relating to them would significantly 
affect code maintainability. Since we have not found any prior work that 
focuses on suggesting refactorings of this subset, we think that we have a 
good motivation for doing this research. Our main contribution is a tool that 
automatically identifies code smells in a codebase that are candidates for 
refactoring. The tool takes as input a Java file and outputs a list of 
refactoring suggestions with line numbers. We have also created an Eclipse 
plugin that offers the same functionality inside Eclipse.


The rest of this paper is organized as follows:
Section~\ref{sec:refactoringpatterns} gives formal definitions of the 
refactoring patterns our tool suggests and rules specifying when code 
matches each pattern. Section~\ref{sec:implementation} describes our tool's 
implementation. In Section~\ref{sec:evaluation} we show our results and the 
conclusions we draw from them. We outline our future work that will augment 
our tool and evaluation in Section~\ref{sec:futurework}. Finally, related 
work is covered in Section~\ref{sec:relatedwork}.


