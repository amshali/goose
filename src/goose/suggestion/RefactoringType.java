package goose.suggestion;

public enum RefactoringType {
  ConsolidateOr("Consolidate Or"),
  ConsolidateAnd("Consolidate And"),
  DecomposeConditional("Decompose Conditional"),
  IntroduceExplainingVariable("Introduce Explaining Variable"), 
  ConsolidateDuplicateConditionalFragments("Consolidate Duplicate Conditional Fragments");
  
  RefactoringType(String name) {
    this.name = name;
  }
  
  public final String name;
}
