package goose.refactorings.cdcf;

import goose.suggestion.RefactoringSuggestion;
import goose.visitors.Context;
import goose.visitors.RefactoringSuggestionVisitor;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.StatementTree;

public class CDCFVisitor extends RefactoringSuggestionVisitor {

  @Override
  public Boolean visitBlock(BlockTree node, Context p) {
    for (StatementTree st : node.getStatements()) {
      if (st instanceof IfTree) {
        CDCFIfVisitor ifVisitor = new CDCFIfVisitor();
        st.accept(ifVisitor, null);
        RefactoringSuggestion rs = ifVisitor.analyze(st, p);
        if (rs != null)
          suggestions.add(rs);
      }
    }
    return super.visitBlock(node, p);
  }

}
