package goose.visitors;

import java.io.File;
import java.io.IOException;

import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.util.JavacTask;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;

public class BranchLinesCounter extends TreeScanner<Boolean, Context> {

  /**
   * @param args
   */
  public static void main(String[] args) {
    Options options = new Options();

    @SuppressWarnings("static-access")
    Option fileOption = OptionBuilder.withArgName("file").hasArg()
        .withDescription("File to analyze").create("file");
    fileOption.setRequired(true);
    options.addOption(fileOption);

    HelpFormatter formatter = new HelpFormatter();
    try {
      CommandLineParser parser = new PosixParser();
      CommandLine cmd = parser.parse(options, args);
      File inputFile = new File(cmd.getOptionValue("file"));
      if (inputFile.exists()) {
        JavaCompiler javaCompiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager javaFileManager = javaCompiler
            .getStandardFileManager(null, null, null);
        JavacTask javacTaskImpl = (JavacTask) javaCompiler.getTask(
            null, javaFileManager, null, null, null,
            javaFileManager.getJavaFileObjects(inputFile));
        try {
          Iterable<? extends CompilationUnitTree> l = javacTaskImpl.parse();
          CompilationUnitTree t = l.iterator().next();
          Context context = new Context();
          context.compilationUnitTree = t;
          context.trees = Trees.instance(javacTaskImpl);
          context.sourcePositions = context.trees.getSourcePositions();
          BranchLinesCounter blc = new BranchLinesCounter();
          t.accept(blc, context);
          System.out.println(blc.numberOfBranchLines);
          System.out.println(blc.numberOfMethodLines);
        } catch (IOException e) {
          e.printStackTrace();
        }
      } else {
        System.err.println("Input file does not exist.");
        System.exit(-1);
      }
      
    } catch (ParseException e1) {
      formatter.printHelp("BranchLinesCounter", options);
    }
  }

  int numberOfBranchLines = 0;
  int numberOfMethodLines = 0;
  
  
  
  
  @Override
  public Boolean visitMethod(MethodTree node, Context context) {
    long startPos = context.sourcePositions.getStartPosition(
        context.compilationUnitTree, node);
    long endPos = context.sourcePositions.getEndPosition(
        context.compilationUnitTree, node);
    long startLineNumber = context.compilationUnitTree.getLineMap()
        .getLineNumber(startPos);
    long endLineNumber = context.compilationUnitTree.getLineMap()
        .getLineNumber(endPos);
    numberOfMethodLines += endLineNumber - startLineNumber + 1;
    return super.visitMethod(node, context);
  }


  @Override
  public Boolean visitIf(IfTree node, Context context) {
    long startPos = context.sourcePositions.getStartPosition(
        context.compilationUnitTree, node);
    long endPos = context.sourcePositions.getEndPosition(
        context.compilationUnitTree, node);
    long startLineNumber = context.compilationUnitTree.getLineMap()
        .getLineNumber(startPos);
    long endLineNumber = context.compilationUnitTree.getLineMap()
        .getLineNumber(endPos);
    numberOfBranchLines += endLineNumber - startLineNumber + 1;
    return true;
  }

}
