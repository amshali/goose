package goose.plugin.views;

import goose.plugin.models.IRSItem;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class RSViewLabelProvider extends LabelProvider implements
    ITableLabelProvider {
  public String getColumnText(Object obj, int index) {
    switch (index) {
      case 0: // Description column
        if (obj instanceof IRSItem)
          return ((IRSItem) obj).getDescription();
        if (obj != null)
          return obj.toString();
        return "";
      case 1: // Path column
        if (obj instanceof IRSItem)
          return ((IRSItem) obj).getResourcePath();
        if (obj != null)
          return obj.toString();
        return "";        
      case 2: // Name column
        if (obj instanceof IRSItem)
          return ((IRSItem) obj).getResourceName();
        return "";
      case 3: // Location column
        if (obj instanceof IRSItem)
          return ((IRSItem) obj).getLocation().toString();
        return "";
      case 4: // Type column
        if (obj instanceof IRSItem)
          return ((IRSItem) obj).getType();
        return "";
      default:
        return "";
    }
  }

  public Image getColumnImage(Object obj, int index) {
    return getImage(obj);
  }

  public Image getImage(Object obj) {
//    return PlatformUI.getWorkbench().getSharedImages()
//        .getImage(ISharedImages.IMG_OBJ_ELEMENT);
    return null;
  }
}