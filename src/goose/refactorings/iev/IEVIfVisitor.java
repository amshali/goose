package goose.refactorings.iev;

import goose.visitors.Context;
import goose.visitors.IfVisitor;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.ErroneousTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.UnaryTree;

public class IEVIfVisitor extends IfVisitor<Void, Context> {
  
  public int complexity = 0;
  
  @Override
  public Void visitIf(IfTree node, Context p) {
    int oldComplexity = this.complexity;
    this.complexity = 0;
    node.getCondition().accept(this, p);
    this.complexity = Math.max(oldComplexity, this.complexity);
    super.visitIf(node, p);
    return null;
  }

  @Override
  public Void visitAnnotation(AnnotationTree node, Context p) {
    complexity += 1;
    return super.visitAnnotation(node, p);
  }

  @Override
  public Void visitMethodInvocation(MethodInvocationTree node, Context p) {
    complexity += 1;
    return super.visitMethodInvocation(node, p);
  }

  @Override
  public Void visitAssignment(AssignmentTree node, Context p) {
    complexity += 1;
    return super.visitAssignment(node, p);
  }

  @Override
  public Void visitCompoundAssignment(CompoundAssignmentTree node, Context p) {
    complexity += 1;
    return super.visitCompoundAssignment(node, p);
  }

  @Override
  public Void visitBinary(BinaryTree node, Context p) {
    complexity += 1;
    return super.visitBinary(node, p);
  }

  @Override
  public Void visitConditionalExpression(ConditionalExpressionTree node,
      Context p) {
    complexity += 1;
    return super.visitConditionalExpression(node, p);
  }

  @Override
  public Void visitErroneous(ErroneousTree node, Context p) {
    complexity += 1;
    return super.visitErroneous(node, p);
  }

  @Override
  public Void visitIdentifier(IdentifierTree node, Context p) {
    complexity += 1;
    return super.visitIdentifier(node, p);
  }

  @Override
  public Void visitArrayAccess(ArrayAccessTree node, Context p) {
    complexity += 1;
    return super.visitArrayAccess(node, p);
  }

  @Override
  public Void visitLiteral(LiteralTree node, Context p) {
    complexity += 1;
    return super.visitLiteral(node, p);
  }

  @Override
  public Void visitNewArray(NewArrayTree node, Context p) {
    complexity += 1;
    return super.visitNewArray(node, p);
  }

  @Override
  public Void visitNewClass(NewClassTree node, Context p) {
    complexity += 1;
    return super.visitNewClass(node, p);
  }

  @Override
  public Void visitParenthesized(ParenthesizedTree node, Context p) {
    complexity += 1;
    return super.visitParenthesized(node, p);
  }

  @Override
  public Void visitMemberSelect(MemberSelectTree node, Context p) {
    complexity += 1;
    return super.visitMemberSelect(node, p);
  }

  @Override
  public Void visitTypeCast(TypeCastTree node, Context p) {
    complexity += 1;
    return super.visitTypeCast(node, p);
  }

  @Override
  public Void visitInstanceOf(InstanceOfTree node, Context p) {
    complexity += 1;
    return super.visitInstanceOf(node, p);
  }

  @Override
  public Void visitUnary(UnaryTree node, Context p) {
    complexity += 1;
    return super.visitUnary(node, p);
  }
  
  
}
