package cceor;

public class TP {
  int test1() {
    for (int i = 0; i < 10; i++) {
      if (true) {
        if (this.m2(1000)) return 0;
        else System.out.println("aaa");
        if (m2(0)) return 0;
        if (m3() > 3) return 0;
        System.out.println("hello");
        if (m2(100)) return 1;
        if (m3() > 100) return 1;
        if (this.m3() > 3) return 3;
        if (m3() > 4) return 2;
        if (m3() > 5) return 2;
        if (m3() > 6) return 2;
      }
    }
    return -1;
  }
  
  void test2() {
    boolean b1 = true, b2 = true, b3 = true, b4 = true;
    if (b1) {
      return;
    }
    if (b2 && b3) {
      return;
    }
    if (b2 || b4) {
      return;
    }
  }
  
  void test3() {
    boolean b1 = true, b2 = true, b3 = true, b4 = true;
    if (b1) {
      System.out.println("test");
      return;
    }
    if (b2 && b3) {
      System.out.println("test");
      return;
    }
    if (b2 || b4) {
      System.out.println("test");
      return;
    }
  }
  
  void test4() throws Exception {
    boolean b1 = true, b2 = true, b3 = true, b4 = true;
    if (b1) {
      System.out.println("test");
      throw new Exception();
    }
    if (b2 && b3) {
      System.out.println("test");
      throw new Exception();
    }
    if (b2 || b4) {
      System.out.println("test");
      throw new Exception();
    }
  }
  
  Object test5() {
    boolean b1 = true, b2 = true, b3 = true, b4 = true;
    if (b1 || b4) {
      System.out.println("test");
      return new Object();
    }
    if (b2 && b3) {
      System.out.println("test");
      return new Object();
    }
    return null;
  }
  
  boolean m2(int x) {
    return true;
  }
  
  int m3() {
    return 0;
  }
  

}
