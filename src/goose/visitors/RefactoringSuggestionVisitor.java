package goose.visitors;

import java.util.ArrayList;
import java.util.List;

import goose.suggestion.RefactoringSuggestion;

import com.sun.source.util.TreeScanner;

public class RefactoringSuggestionVisitor extends TreeScanner<Boolean, Context> {
  protected List<RefactoringSuggestion> suggestions = new ArrayList<RefactoringSuggestion>();

  final public List<RefactoringSuggestion> getSuggestions() {
    return suggestions;
  }

  public void reset() {
    suggestions.clear();
  }
}
