package goose;

import goose.refactorings.cce.ConsolidateAndVisitor;
import goose.refactorings.cce.ConsolidateOrVisitor;
import goose.refactorings.cdcf.CDCFVisitor;
import goose.refactorings.dc.DecomposeConditional;
import goose.refactorings.iev.IEVVisitor;
import goose.suggestion.RefactoringSuggestion;
import goose.visitors.Context;
import goose.visitors.RefactoringSuggestionVisitor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.util.JavacTask;
import com.sun.source.util.Trees;

/**
 * Main class.
 */
public class Main {

  public static void main(String[] args) {
    Main m = new Main();
    List<RefactoringSuggestion> refsList = m.call(args);
    for (RefactoringSuggestion rs : refsList) {
      System.out.println(rs);
    }
  }

  public List<RefactoringSuggestion> call(String[] args) {
    Options options = new Options();

    options.addOption("cceor", true, "Consolidate Conditional Expression Or");
    options.addOption("cceand", true, "Consolidate Conditional Expression And");
    options.addOption("cdcf", false,
        "Consolidate Duplicate Conditional Fragments");
    options.addOption("dc", true, "Decompose Conditional");
    options.addOption("iev", true, "Introduce Explaining Variable");

    @SuppressWarnings("static-access")
    Option fileOption = OptionBuilder.withArgName("file").hasArg()
        .withDescription("File to analyze").create("file");
    fileOption.setRequired(true);
    options.addOption(fileOption);

    HelpFormatter formatter = new HelpFormatter();
    try {
      CommandLineParser parser = new PosixParser();
      CommandLine cmd = parser.parse(options, args);

      List<RefactoringSuggestionVisitor> visitors = new ArrayList<>();

      if (cmd.hasOption("dc")) {
        visitors.add(new DecomposeConditional(Integer.parseInt(cmd
            .getOptionValue("dc"))));
      }
      if (cmd.hasOption("iev")) {
        visitors
            .add(new IEVVisitor(Integer.parseInt(cmd.getOptionValue("iev"))));
      }
      if (cmd.hasOption("cdcf")) {
        visitors.add(new CDCFVisitor());
      }
      if (cmd.hasOption("cceand")) {
        visitors.add(new ConsolidateAndVisitor(Integer.parseInt(cmd
            .getOptionValue("cceand"))));
      }
      if (cmd.hasOption("cceor")) {
        visitors.add(new ConsolidateOrVisitor(Integer.parseInt(cmd
            .getOptionValue("cceor"))));
      }

      File inputFile = new File(cmd.getOptionValue("file"));
      if (inputFile.exists()) {
        JavaCompiler javaCompiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager javaFileManager = javaCompiler
            .getStandardFileManager(null, null, null);
        JavacTask javacTaskImpl = (JavacTask) javaCompiler.getTask(
            null, javaFileManager, null, null, null,
            javaFileManager.getJavaFileObjects(inputFile));
        try {
          Iterable<? extends CompilationUnitTree> l = javacTaskImpl.parse();
          CompilationUnitTree t = l.iterator().next();
          Context context = new Context();
          context.compilationUnitTree = t;
          context.trees = Trees.instance(javacTaskImpl);
          context.sourcePositions = context.trees.getSourcePositions();
          List<RefactoringSuggestion> refsList = runRefactoringSuggestions(
              visitors, t, context);
          return refsList;
        } catch (IOException e) {
          e.printStackTrace();
        }
      } else {
        System.err.println("Input file does not exist.");
        System.exit(-1);
      }

    } catch (ParseException e1) {
      formatter.printHelp("Goose", options);
    }
    return new ArrayList<>();
  }

  public static List<RefactoringSuggestion> runRefactoringSuggestions(
      List<RefactoringSuggestionVisitor> visitors, CompilationUnitTree cu,
      Context p) {
    List<RefactoringSuggestion> refactoringSuggestions = new ArrayList<>();
    System.out.println("File: " + cu.getSourceFile().getName());
    for (RefactoringSuggestionVisitor v : visitors) {
      cu.accept(v, p);
      refactoringSuggestions.addAll(v.getSuggestions());
    }
    return refactoringSuggestions;
  }

}
