package goose.plugin.views;

import goose.plugin.models.IRSItem;
import goose.plugin.models.RSManager;
import goose.plugin.models.RSManagerListener;
import goose.plugin.models.RSManagerEvent;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;

public class RSViewContentProvider implements
    IStructuredContentProvider, RSManagerListener {
  private TableViewer viewer;
  private RSManager manager;

  @Override
  public void dispose() {
  }

  @Override
  public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
    this.viewer = (TableViewer) viewer;
    if (manager != null)
      manager.removeManagerListener(this);
    manager = (RSManager) newInput;
    if (manager != null)
      manager.addManagerListener(this);
  }

  @Override
  public IRSItem[] getElements(Object inputElement) {
    return manager.getRefactoringSuggestions();
  }

  @Override
  public void refactoringSuggestionsChanged(
      RSManagerEvent event) {
    viewer.getTable().setRedraw(false);
    try {
      viewer.remove(event.getRemoved().toArray());
      viewer.add(event.getAdded().toArray());
    } finally {
      viewer.getTable().setRedraw(true);
    }    
  }

}
