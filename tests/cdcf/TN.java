package cdcf;

/**
 * CDCF true negative tests.
 * 
 * @author QualityCan
 */
public class TN {
  public boolean b;
  public int i;
  public double d;
  
  /**
   * Non-collectively exhaustive branches.
   */
  public void test1() {
    if (b) {
      System.out.println("B");
    } else if (b) {
      System.out.println("B");
    }
  }
}
