package iev;

/**
 * IEV false positive tests.
 * 
 * @author QualityCan
 */
public class FP {
  public boolean b;
  public int i;
  public double d;
  
  /**
   * Side effects.
   */
  public void test1() {
    if (sideEffect() > 0 || sideEffect() > 1 || sideEffect() > 2 || 
        sideEffect() > 3 || sideEffect() > 4 || sideEffect() > 5 || 
        sideEffect() > 6 || sideEffect() > 7 || sideEffect() > 8 || 
        sideEffect() > 9) {
      System.out.println("Hello.");
    }
  }
  
  private int sideEffect() {
    this.i++;
    return this.i;
  }

  /**
   * No subexpressions.
   */
  public void test2() {
    if (i >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 
        1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 
        1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 >> 1 > 0) {
      System.out.println("Hello.");
    }
  }

  /**
   * Short subexpressions.
   */
  public void test3() {
    if (b || !b || b || !b || b || !b || b || !b || b || !b || b || !b || b
        || !b || b || !b || b || !b || b || !b || b || !b || b || !b || b || !b
        || b || !b || b || !b || b || !b || b || !b || b || !b || b || !b || b
        || !b || b || !b || b || !b || b || !b || b || !b || b || !b || b || !b
        || b || !b || b || !b || b || !b || b || !b || b || !b || b || !b || b
        || !b || b || !b || b || !b || b || !b)
      ;
    else
      ;
  }

  /**
   * Excessive parenthesis.
   */
  public void test4() {
    if ((((((((((((((((((((((((((((((((((b))))))))))))))))))))))))))))))))))
      ;
    else
      ;
  }
}
