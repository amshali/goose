package goose.plugin.models;

import java.util.Collection;
import java.util.EventObject;

public class RSManagerEvent extends EventObject {
  private static final long serialVersionUID = 2387623736284468300L;

  private final Collection<IRSItem> added;
  private final Collection<IRSItem> removed;

  public RSManagerEvent(RSManager source, Collection<IRSItem> r,
      Collection<IRSItem> itemsAdded) {
    super(source);
    removed = r;
    added = itemsAdded;
  }

  public Collection<IRSItem> getAdded() {
    return added;
  }

  public Collection<IRSItem> getRemoved() {
    return removed;
  }

}
