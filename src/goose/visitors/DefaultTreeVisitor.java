package goose.visitors;


import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.ArrayTypeTree;
import com.sun.source.tree.AssertTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.BreakTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.DoWhileLoopTree;
import com.sun.source.tree.EmptyStatementTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ErroneousTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.SwitchTree;
import com.sun.source.tree.SynchronizedTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.UnaryTree;
import com.sun.source.tree.UnionTypeTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.tree.WildcardTree;

public abstract class DefaultTreeVisitor extends RefactoringSuggestionVisitor {

  protected Boolean defaultAction(Tree t, Context Context) {
    return null;
  }

  @Override
  public Boolean visitAnnotation(AnnotationTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitMethodInvocation(MethodInvocationTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitAssert(AssertTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitAssignment(AssignmentTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitCompoundAssignment(CompoundAssignmentTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitBinary(BinaryTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitBlock(BlockTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitBreak(BreakTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitCase(CaseTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitCatch(CatchTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitClass(ClassTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitConditionalExpression(ConditionalExpressionTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitContinue(ContinueTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitDoWhileLoop(DoWhileLoopTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitErroneous(ErroneousTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitExpressionStatement(ExpressionStatementTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitEnhancedForLoop(EnhancedForLoopTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitForLoop(ForLoopTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitIdentifier(IdentifierTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitIf(IfTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitImport(ImportTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitArrayAccess(ArrayAccessTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitLabeledStatement(LabeledStatementTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitLiteral(LiteralTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitMethod(MethodTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitModifiers(ModifiersTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitNewArray(NewArrayTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitNewClass(NewClassTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitParenthesized(ParenthesizedTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitReturn(ReturnTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitMemberSelect(MemberSelectTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitEmptyStatement(EmptyStatementTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitSwitch(SwitchTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitSynchronized(SynchronizedTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitThrow(ThrowTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitCompilationUnit(CompilationUnitTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitTry(TryTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitParameterizedType(ParameterizedTypeTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitUnionType(UnionTypeTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitArrayType(ArrayTypeTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitTypeCast(TypeCastTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitPrimitiveType(PrimitiveTypeTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitTypeParameter(TypeParameterTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitInstanceOf(InstanceOfTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitUnary(UnaryTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitVariable(VariableTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitWhileLoop(WhileLoopTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitWildcard(WildcardTree node, Context Context) {

    return defaultAction(node, Context);
  }

  @Override
  public Boolean visitOther(Tree node, Context Context) {

    return defaultAction(node, Context);
  }
}
