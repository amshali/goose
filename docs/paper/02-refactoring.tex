\section{Refactoring Patterns}
\label{sec:refactoringpatterns}
Each refactoring has a general pattern that can be matched against code in a 
codebase. In the following subsections we formally specify these 
patterns and give concrete examples of each. Our tool could potentially report 
a lot of suggestions for each pattern, which may be unmanageable for a 
developer to look at. Therefore, for most patterns we have user-defined 
threshold values that limit the number of refactorings our tool reports. 
Before we explain each pattern, we need to define the notion of 
\textit{Identical Statements}. We define two statements to be identical if 
their texts match each other character by character. Identical statements are 
used to locate some of the patterns.


\subsection{Consolidate Conditional Expression (CCE)}
This refactoring eliminates code duplication by removing superfluous 
\lstinline{if} statements. There are two types of this refactoring; the first 
deals with the logical \lstinline{OR}, and the second deals with the logical 
\lstinline{AND}. Although they look quite different in code, they can be 
understood and implemented in a similar way, hence why they are grouped 
together.


\subsubsection{OR Consolidation}
Sometimes code contains sequences of \lstinline{if} statements without 
matching \lstinline{else} clauses that all yield the same result. Code of 
this type is logically equivalent to a single \lstinline{if} statement 
containing each conditional expression separated by \lstinline{||}. 
Figure~\ref{fig:CCEOR-example} shows an example of this type of refactoring.


\begin{figure}[htb]
\begin{lstlisting}
  double disabilityAmount() {
          if (_seniority < 2) return 0;
          if (_monthsDisabled > 12) return 0;
          if (_isPartTime) return 0;
          // compute the disability amount
\end{lstlisting}
\center{\huge{$\Downarrow$}}
\begin{lstlisting}
  double disabilityAmount() {
          if (_seniority < 2 || _monthsDisabled > 12 || _isPartTime)
            return 0;
          // compute the disability amount
\end{lstlisting}
\caption{Consolidate conditional expression (OR) example}
\label{fig:CCEOR-example}
\end{figure}


In the example in Figure~\ref{fig:CCEOR-example}, there are three 
\lstinline{if} statements, all with the same result. The refactoring cleans 
up the code by reducing it to a single construct. The general code pattern 
for this refactoring is presented in Figure~\ref{fig:CCEOR-pattern}.


\begin{figure}[htb]
\begin{lstlisting}
if (<Cond1>) {<Statements>}
if (<Cond2>) {<Statements>}
...
if (<CondN>) {<Statements>}
\end{lstlisting}
\center{\huge{$\Downarrow$}}
\begin{lstlisting}
if ((<Cond1>) || (<Cond2>) || ... || (<CondN>)) {
 <Statements>
}
\end{lstlisting}
\caption{Consolidate conditional expression (OR) general pattern}
\label{fig:CCEOR-pattern}
\end{figure}


Drawing from the pattern, we establish the following rules that are required 
for a piece of code to be a candidate for this type of refactoring:


\begin{enumerate}
\item Each of \lstinline{<Statements>} must be identical
\item No \lstinline{if} statement can have an \lstinline{else} branch
\item \lstinline{<Statements>} must end with either \lstinline{return} or 
\lstinline{throw}
\end{enumerate}


It is obvious why we need the first two rules. To clarify the third rule, 
let us look at an example. Figure~\ref{fig:CCEOR-return-why} shows a code 
fragment that initializes a variable \lstinline{x} to 0 followed by two 
\lstinline{if} statements and a print statement. The statements inside both 
\lstinline{if} statements are identical. When both conditions are true, the 
variable \lstinline{x} will be incremented twice and the print function will 
print 2 as a result. After refactoring this piece of code according to this 
pattern, even if both conditions are true, the variable \lstinline{x} will be 
incremented only once, which shows that it is not equivalent to the original. 
In contrast, if \lstinline{<Statements>} ends with a return or throw, the 
control will be terminated at that point will not fall through to consecutive 
\lstinline{if}s.


\begin{figure}[htb]
\begin{lstlisting}
x = 0;
if (cond1) {x++;}
if (cond2) {x++;}
print(x); // will print 2
\end{lstlisting}
\center{\huge{$\Downarrow$}}
\begin{lstlisting}
if (cond1 || cond2) {
  x++;
}
print(x); // will print 1
\end{lstlisting}
\caption{An example showing why each if branch must end in a return or throw}
\label{fig:CCEOR-return-why}
\end{figure}


\subsubsection{AND Consolidation}
The \lstinline{AND} case differs from the \lstinline{OR} case in that it deals 
with with nested instead of consecutive \lstinline{if} statements. The result 
of the refactoring is therefore each conditional expression being joined 
using \lstinline{AND} instead of \lstinline{OR}. See the general form of the 
pattern in Figure~\ref{fig:CCEAND-pattern}.


\begin{figure}[htb]
\begin{lstlisting}
if (<Cond1>) {
  if (<Cond2>) {
    ...
    if (<CondN>) {
      <Statements>
    }
  }
}
\end{lstlisting}
\center{\huge{$\Downarrow$}}
\begin{lstlisting}
if ((<Cond1>) && (<Cond2>) && ... && (<CondN>)) {
  <Statements>
}
\end{lstlisting}
\caption{Consolidate conditional expression refactoring (AND) general pattern}
\label{fig:CCEAND-pattern}
\end{figure}


The rules for the refactoring are simpler than the \lstinline{OR} case. Due 
to the fact that there is only one set of statements, we can eliminate the 
first and third rules. Therefore, the only requirement is that there are no 
\lstinline{else} statements present in the nested block.


\subsubsection{Threshold}
This refactoring pattern has an obvious threshold for suggestion that is the 
same for the \lstinline{OR} and \lstinline{AND} types. If the number of 
\lstinline{if} statements which are chained together (either sequentially or 
nested) is greater than or equal to the threshold, our tool makes a 
suggestion. Otherwise we ignore it, even if it matches the pattern. This is 
useful because \lstinline{if} chains of two clauses are much more common in 
code than chains of three or more, and thus setting the threshold higher than 
two would prune a large number of suggestions.


\subsection{Decompose Conditional (DC)}
This refactoring involves a complex conditional expression in which multiple 
method calls are made on the same object. In this case, we can suggest 
refactoring the calls on the object into private methods. 
See Figure~\ref{fig:DC-example} for an example of this refactoring.


\begin{figure}[htb]
\begin{lstlisting}
if (date.before (SUMMER_START) || date.after(SUMMER_END))
  charge = quantity * _winterRate + _winterServiceCharge;
else charge = quantity * _summerRate;
\end{lstlisting}
\center{\huge{$\Downarrow$}}
\begin{lstlisting}
if (notSummer(date))
  charge = winterCharge(quantity);
else charge = summerCharge (quantity);
\end{lstlisting}
\caption{An example of a decompose conditional refactoring}
\label{fig:DC-example}
\end{figure}


In the example, the conditional expression inside the \lstinline{if} 
statement is refactored to a method called \lstinline{notSummer}. There are 
two advantages to this. First, the new \lstinline{if} statement reads much 
better, and the \lstinline{notSummer} condition is now modularized so that 
it can be invoked multiple times.


\subsubsection{Threshold}
For the purpose of this refactoring we define the complexity of a 
conditional expression to be the maximum number of method calls on a single 
object. The threshold is thus the minimum complexity required to suggest 
the code pattern. For instance, the example in Figure~\ref{fig:DC-example} 
has a complexity of two, since two calls are invoked on the \lstinline{date} 
object. Therefore, this code would be suggested for a threshold of two, but 
not for a threshold of three or higher.


\subsection{Consolidate Duplicate Conditional Fragments (CDCF)}
Sometimes a fragment of code is duplicated at the tail end of each branch 
of an \lstinline{if-else} construct. This refactoring consolidates 
the duplicated fragments to just after the end of the construct. The rationale 
behind this refactoring is that the control flow logic becomes easier to 
understand and that the code size becomes smaller. See 
Figure~\ref{fig:CDCF-example} for an example of this type of refactoring.


\begin{figure}[htb]
\begin{lstlisting}
if (isSpecialDeal()) {
  total = price * 0.95;
  send();
}
else {
  total = price * 0.98;
  send();
}
\end{lstlisting}
\center{\huge{$\Downarrow$}}
\begin{lstlisting}
if (isSpecialDeal())
  total = price * 0.95;
else
  total = price * 0.98;
send();
\end{lstlisting}
\caption{Consolidate duplicate conditional fragments example}
\label{fig:CDCF-example}
\end{figure}


We have generalized this refactoring to include consolidating code fragments
occurring at the beginning of each branch to just before the expression.
Although less common, it serves as the complement to the tail case that makes
the refactoring more complete. The generalized form is presented in
Figure~\ref{fig:CDCF-pattern}. In the figure, all the 
\lstinline{<BStatements>} are identical as well as all the 
\lstinline{<EStatements>}. Branch-unique code may occur between them, which 
is not moved.


\begin{figure}[htb]
\begin{lstlisting}
if (<Cond1>) {
  <BStatements>
  <Unique-Statements-1>
  <EStatements>
}
else if (<Cond2>) {
  <BStatements>
  <Unique-Statements-2>
  <EStatements>
}
...
} else {
  <BStatements>
  <Unique-Statements-N>
  <EStatements>
}
\end{lstlisting}
\center{\huge{$\Downarrow$}}
\begin{lstlisting}
<BStatements>
if (<Cond1>) {
  <Unique-Statements-1>
}
else if (<Cond2>) {
  <Unique-Statements-2>
}
  ...
} else {
  <Unique-Statements-N>
}
<EStatements>
\end{lstlisting}
\caption{Consolidate duplicate conditional fragments general pattern}
\label{fig:CDCF-pattern}
\end{figure}


There is one rule that affects the \lstinline{<BStatements>} suggestion. 
We cannot move \lstinline{<BStatements>} if any \lstinline{<Cond>} 
contains statements that change program state, such as a method calls. 
This is because it would change the semantics of the program by invoking 
such statements after instead of before \lstinline{<BStatements>}. This rule 
does not affect \lstinline{<EStatements>} suggestions.


\subsection{Introduce Explaining Variable (IEV)}
Conditional expressions in imperative languages are prone to excessive 
complexity. This kind of refactoring seeks to break down complicated 
conditionals by extracting some of the code into local variables before the 
conditional. From experience, we recognize that conditional complexity most 
often arises from code being joined using logical AND and OR, therefore we 
focus on extracting code located between these operators. We define a 
subexpression to be a continuous piece of code within a conditional expression 
that is surrounded by, but does not contain, \lstinline{AND} or \lstinline{OR} 
operators. Thus, a conditional expression can be simplified by moving 
subexpressions to local variables before the conditional. 
Figure~\ref{fig:IEV-example} shows an example of this technique.


\begin{figure}[htb]
\begin{lstlisting}
if ((platform.toUpperCase().indexOf("MAC") > -1) &&
        (browser.toUpperCase().indexOf("IE") > -1) &&
        wasInitialized() && resize > 0 ) {
 // do something
}
\end{lstlisting}
\center{\huge{$\Downarrow$}}
\begin{lstlisting}
final boolean isMacOs         = platform.toUpperCase().indexOf("MAC") > -1;
final boolean isIEBrowser = browser.toUpperCase().indexOf("IE")  > -1;
final boolean wasResized  = resize > 0;
if (isMacOs && isIEBrowser && wasInitialized() && wasResized) {
 // do something
}
\end{lstlisting}
\caption{Introduce explaining variable example}
\label{fig:IEV-example}
\end{figure}


In the original code, the length of the conditional necessitated ad hoc 
indenting in order to keep the line from becoming too long. Additionally, it 
is not immediately obvious when the conditional evaluates to \lstinline{true}. 
The refactored code solves both problems. The conditional expression is 
small enough to fit on a single line and can be read much like an ordinary 
sentence. Readers can determine what each subexpression means by looking at 
the introduced variables. The general code pattern for this refactoring is 
presented in Figure~\ref{fig:IEV-pattern} below.


\begin{figure}[htb]
\begin{lstlisting}
if (<Expr1>) {<Statements>}
else if (<Expr2>) {<Statements>}
...
else if (<ExprN>) {<Statements>}
\end{lstlisting}
\center{\huge{$\Downarrow$}}
\begin{lstlisting}
final boolean <Name1> = <SubExpr1>;
final boolean <Name2> = <SubExpr2>;
...
final boolean <NameN> = <SubExprN>;
if ...
\end{lstlisting}
\caption{Introduce explaining variable general pattern}
\label{fig:IEV-pattern}
\end{figure}


As discussed, each \lstinline{<Expr>} in the original code can be broken down 
into subexpressions separated by logical \lstinline{AND}s and \lstinline{OR}s. 
These subexpressions are refactored as \lstinline{<SubExprs>}. Explaining 
variables are listed in the order in which they were extracted from the 
\lstinline{<Exprs>}. Each \lstinline{<Name>} is specified by the programmer, 
and should explain what the corresponding subexpression is specifying. 
Although not clear from the diagram, we handle nested \lstinline{if-else} 
blocks even if there are intermediate blocks delimited by curly brackets. 
Similar to other conditional expression refactorings, side effects caused by 
short-circuiting can pose a problem. In this case, due to the complexity of 
the analysis required to ensure a safe recommendations, we leave it up to the 
programmer implementing the refactoring to ensure that there is no problem.


\subsubsection{Threshold}
In our implementation of this refactoring, we assign an integer to all 
matching code fragments that indicates their complexity. The integer does not 
lend itself to an easy definition; it is a heuristic that roughly counts the 
number of objects, method calls, and operators in each subexpression. Our 
threshold for this refactoring simply specifies a minimum complexity.


