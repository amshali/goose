package goose.plugin.models;

import goose.suggestion.RefactoringSuggestion;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.Platform;

public class RSResource implements IRSItem {

  private RefactoringSuggestion suggestion;
  private IResource resource;

  public RSResource(RefactoringSuggestion suggestion, IResource resource) {
    super();
    this.suggestion = suggestion;
    this.resource = resource;
  }

  @Override
  public Object getAdapter(@SuppressWarnings("rawtypes") Class adapter) {
    if (adapter.isInstance(resource))
      return resource;
    return Platform.getAdapterManager().getAdapter(this, adapter);
  }

  @Override
  public String getDescription() {
    return suggestion.getType().name;
  }

  @Override
  public Integer getLocation() {
    return suggestion.getStartLineNumber().intValue();
  }

  @Override
  public boolean isSuggestionFor(IResource obj) {
    return resource.equals(obj);
  }

  @Override
  public String getType() {
    return suggestion.getType().name();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((resource == null) ? 0 : resource.hashCode());
    result = prime * result
        + ((suggestion == null) ? 0 : suggestion.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    RSResource other = (RSResource) obj;
    if (resource == null) {
      if (other.resource != null)
        return false;
    } else if (!resource.equals(other.resource))
      return false;
    if (suggestion == null) {
      if (other.suggestion != null)
        return false;
    } else if (!suggestion.equals(other.suggestion))
      return false;
    return true;
  }

  @Override
  public String getResourceName() {
    return resource.getName();
  }

  @Override
  public String getResourcePath() {
    return resource.getParent().getFullPath().toString();
  }

}
