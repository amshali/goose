package goose.refactorings.cce;

import goose.suggestion.RefactoringSuggestion;
import goose.visitors.Context;
import goose.visitors.RefactoringSuggestionVisitor;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.StatementTree;

import java.util.ArrayList;
import java.util.List;

public class ConsolidateOrVisitor extends RefactoringSuggestionVisitor {
  private int MIN_IFS = 2;

  public ConsolidateOrVisitor(int mIN_IFS) {
    super();
    MIN_IFS = mIN_IFS;
  }

  List<RefactoringSuggestion> refactoringSuggestions = new ArrayList<RefactoringSuggestion>();

  @Override
  public Boolean visitMethod(MethodTree node, Context p) {
    if (node.getBody() != null)
      node.getBody().accept(this, p);
    return true;
  }

  @Override
  public Boolean visitBlock(BlockTree block, Context p) {
    ConsolidateOrStateMachine stateMachine = new ConsolidateOrStateMachine(
        MIN_IFS);
    block.accept(stateMachine, p);
    suggestions.addAll(stateMachine.getSuggestions());
    for (StatementTree st : block.getStatements()) {
      st.accept(this, p);
    }
    return true;
  }
}
