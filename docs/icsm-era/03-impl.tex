\section{Implementation}
\label{sec:implementation}
We implemented our tool leveraging the Java compiler tree API, which generates
and provides access to abstract syntax trees (ASTs) from Java files.
This access is provided via the visitor design pattern. This design pattern
allows us to walk ASTs in a custom way specific to each refactoring type.
For the types we implemented, we use two visitors. The first visitor invokes 
the second visitor for each AST node type relevant to the pattern. The second 
visitor visits the conditional expressions and statements of 
\lstinline{if-else} branches and gathers information about them. 
The visitor then matches the information with the pattern. If the code 
matches, the first visitor checks whether matched code passes the required 
threshold. If it does, the tool will write a refactoring suggestion for that 
point in the code. Figure~\ref{fig:IEVVisitor} shows the first visitor 
that is part of the introduce explaining variable implementation.


\begin{figure}[htb]
\begin{lstlisting}
// First visitor
public class IEVVisitor
    extends TreeScanner<Boolean, Context> {
  // The threshold for this pattern
  private int COMPLEXITY_THRESHOLD = 30;
  public IEVVisitor(int cOMPLEXITY_THRESHOLD) {
    super();
    COMPLEXITY_THRESHOLD = cOMPLEXITY_THRESHOLD;
  }
  public Boolean visitBlock(BlockTree node, Context p){
    for (StatementTree st : node.getStatements()) {
      if (st instanceof IfTree) {
        // The second visitor
        IEVIfVisitor ifVisitor = new IEVIfVisitor();
        st.accept(ifVisitor, null);
        // Checking the threshold
        if (ifVisitor.complexity>COMPLEXITY_THRESHOLD){
          // Creating a suggestion
          RefactoringSuggestion rs = 
            RefactoringSuggestion.create(p, st, st,
            RefactoringType.IntroduceExplainingVariable);
          // Writing out the suggestion
          System.out.println(rs);
        }
      }
    }
    return super.visitBlock(node, p);
  }
}
\end{lstlisting}
\caption{Introduce explaining variable visitor implementation}
\label{fig:IEVVisitor}
\end{figure}


\subsection{Eclipse Plugin}
We implemented an Eclipse plugin that serves as a GUI front-end for our 
command line tool. The plugin was implemented and tested with 
Eclipse version 3.7.0. Figure~\ref{fig:plugin} shows a snapshot of tbe
plugin. As can be seen in the figure, the plugin offers a view called 
\textit{Goose} that holds a list of refactoring suggestions. In order to 
populate this view with suggestions for a particular Java file, the user must 
right click on the Java file and select \textit{Suggest Refactorings} from 
the Goose sub-menu. Tbe user can double click on a suggestion to jump to the 
file and line to which the suggestion is referring to.


\begin{figure*}[htb]
\centering
	\includegraphics[scale=0.48]{plugin.png}
\caption{Snapshot of our tool Eclipse plugin code named \textit{Goose}}
\label{fig:plugin}
\end{figure*}


\subsection{Discussion}
To the best of our knowledge, most Java analysis frameworks work on ASTs. 
It is important to note that although we do make use of ASTs, we do not rely 
on symbol table information. Constructing the symbol table in Java requires
resolving all the library dependencies. This means that a single Java file 
cannot be processed independently if it has dependencies to other Java files 
and libraries. Therefore, not using the symbol table gives us the advantage 
of analyzing a Java file independent of all of its dependencies. 


On the other hand, not using the symbol table information has its own 
disadvantages. For instance, suppose there is a field \lstinline{x} in a 
class. This field can be referred by \lstinline{x} or by \lstinline{this.x}. 
From a syntactic analysis point of view \lstinline{x} and \lstinline{this.x} 
are two different things and they have different ASTs. However, they will have 
one symbol instance in the symbol table. Since our implementation do not 
use symbol table, it will fail to recognize the fact that \lstinline{x} and 
\lstinline{this.x} are actually the same variable. The consequence of this 
is that we consider \lstinline{System.out.println(x);} and 
\lstinline{System.out.println(this.x);} two different statements and that will 
increase our false negatives on CDCF pattern. We have not encountered any 
instance of such issue, but we need to note that this current implementation 
is prone to it. One last disadvantage with our technique is that it is 
impossible to perform analysis across multiple Java files. For example, 
we are not able to specify refactoring patterns involving manipulations of 
an inheritance heirarchy.
 
 
 