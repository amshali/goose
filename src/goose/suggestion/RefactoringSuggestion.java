package goose.suggestion;

import java.util.Random;

import goose.visitors.Context;

import com.sun.source.tree.Tree;

public abstract class RefactoringSuggestion {
  long startLineNumber;
  long endLineNumber;

  protected RefactoringType type;

  protected RefactoringSuggestion() {
    setType();
  }

  public RefactoringSuggestion(Context c, Tree start, Tree end) {
    this();
    long startPos = c.sourcePositions.getStartPosition(c.compilationUnitTree,
        start);
    long endPos = c.sourcePositions
        .getStartPosition(c.compilationUnitTree, end);
    this.startLineNumber = c.compilationUnitTree.getLineMap().getLineNumber(
        startPos);
    this.endLineNumber = c.compilationUnitTree.getLineMap().getLineNumber(
        endPos);
  }

  protected abstract void setType();

  public RefactoringType getType() {
    return type;
  }

  @Override
  public String toString() {
    return "type=" + getType() + ", startLineNumber=" + startLineNumber
        + ", endLineNumber=" + endLineNumber;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (int) (endLineNumber ^ (endLineNumber >>> 32));
    result = prime * result
        + (int) (startLineNumber ^ (startLineNumber >>> 32));
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    RefactoringSuggestion other = (RefactoringSuggestion) obj;
    if (endLineNumber != other.endLineNumber)
      return false;
    if (startLineNumber != other.startLineNumber)
      return false;
    if (type != other.type)
      return false;
    return true;
  }

  public Long getStartLineNumber() {
    return startLineNumber;
  }

  public void setStartLineNumber(Long startLineNumber) {
    this.startLineNumber = startLineNumber;
  }

  public Long getEndLineNumber() {
    return endLineNumber;
  }

  public void setEndLineNumber(Long endLineNumber) {
    this.endLineNumber = endLineNumber;
  }

  public static RefactoringSuggestion getADummyRefactoringSuggestion() {
    RefactoringSuggestion r = new RefactoringSuggestion() {
      @Override
      public void setType() {
        type = RefactoringType.ConsolidateAnd;
      }
    };
    r.setStartLineNumber((long) new Random().nextInt(1000));
    return r;
  }

}
