package goose.plugin.popup.actions;

import goose.Main;
import goose.plugin.models.IRSItem;
import goose.plugin.models.RSManager;
import goose.plugin.models.RSResource;
import goose.suggestion.RefactoringSuggestion;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

public class SuggestRefactoringsAction implements IObjectActionDelegate {

  private Shell shell;
  private ISelection selection;

  /**
   * Constructor for Action1.
   */
  public SuggestRefactoringsAction() {
    super();
  }

  /**
   * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
   */
  public void setActivePart(IAction action, IWorkbenchPart targetPart) {
    shell = targetPart.getSite().getShell();
  }

  /**
   * @see IActionDelegate#run(IAction)
   */
  public void run(IAction action) {
//    MessageDialog.openInformation(
//    shell,
//    "Goose",
//    "New Action was executed.");
    final IStructuredSelection s = (IStructuredSelection) selection;
    s.getFirstElement();
    final Object selected = s.getFirstElement();
    if (selected instanceof IResource
        && ((IResource) selected).getFullPath().toString().endsWith(".java")) {
      Main refMain = new Main();

      List<RefactoringSuggestion> refsList = 
          refMain.call(new String[] { "-cceand", "2", "-cceor", "2", "-iev", "20",
          "-dc", "2", "-cdcf", "-file",
          "\""+((IResource) selected).getLocation().toOSString()+"\"" });
      List<IRSItem> irsItems = new ArrayList<>();
      for (RefactoringSuggestion rs : refsList) {
        irsItems.add(new RSResource(rs, (IResource) selected));
      }
      RSManager.getManager().addRefactoringSuggestions((IResource) selected, irsItems);
    }
  }

  /**
   * @see IActionDelegate#selectionChanged(IAction, ISelection)
   */
  public void selectionChanged(IAction action, ISelection selection) {
    this.selection = selection;
  }

}
