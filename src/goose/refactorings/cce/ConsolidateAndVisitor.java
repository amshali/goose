package goose.refactorings.cce;

import goose.suggestion.CCEAndRefactoringSuggestion;
import goose.suggestion.RefactoringSuggestion;
import goose.visitors.Context;
import goose.visitors.RefactoringSuggestionVisitor;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.StatementTree;

public class ConsolidateAndVisitor extends RefactoringSuggestionVisitor {

  private int MIN_IFS;

  public ConsolidateAndVisitor(int mIN_IFS) {
    super();
    MIN_IFS = mIN_IFS;
  }

  @Override
  public Boolean visitIf(IfTree node, Context p) {
    IfTree currentTree = node;
    StatementTree endingTree;
    int count = 0;
    while (true) {
      if (currentTree.getElseStatement() == null) {
        count++;
        StatementTree thenTree = currentTree.getThenStatement();
        if (thenTree instanceof IfTree) {
          currentTree = (IfTree) thenTree;
        } else if (thenTree instanceof BlockTree) {
          BlockTree blockTree = (BlockTree) thenTree;
          if (blockTree.getStatements().size() == 1
              && blockTree.getStatements().get(0) instanceof IfTree) {
            currentTree = (IfTree) blockTree.getStatements().get(0);
          } else {
            endingTree = thenTree;
            break;
          }
        } else {
          endingTree = thenTree;
          break;
        }
      } else {
        endingTree = currentTree;
        break;
      }
    }
    if (count >= MIN_IFS) {
      RefactoringSuggestion rs = new CCEAndRefactoringSuggestion(p, node,
          currentTree);
      suggestions.add(rs);
    }
    if (endingTree instanceof IfTree) {
      currentTree.getThenStatement().accept(this, p);
      currentTree.getElseStatement().accept(this, p);
    } else {
      endingTree.accept(this, p);
    }
    return true;
  }

}
