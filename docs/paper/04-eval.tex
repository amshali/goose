\section{Evaluation}
\label{sec:evaluation}
Our main goal is to show that there are no factors that limit the usability 
of our tool. To help us answer this goal, we have derived the following 
metrics, each of which is analysed in subsequent sections.
\begin{enumerate}
\item The number of suggestions our tool can find on real-world projects
\item Precision and recall
\item Runtime performance
\end{enumerate}


\subsection{Suggestion Data}
Our first goal was to determine if our tool finds enough refactorings to 
warrant its use. We have seen that code is dense with \lstinline{if-else} 
constructs. However, this does not tell us if our refactoring patterns 
are common enough in code such that finding them has practical value. 
To determine this, we selected several open-source Java projects as input 
to our tool with the goal of measuring the number of suggestions our tool 
makes for each one. The primary criteria for project selection was the 
likelihood of the existence of refactoring opportunities. This is not directly 
measurable, but may be inferred from other indicator statistics. Size 
is the most obvious one; more code means more opportunities. Project age is 
another indicator since it implies the addition of new 
features over time that were not envisioned during the original 
architecture. One last indicator is the number of distinct developers; more 
developers may increase the number of code smells due to inexperience or 
differing design philosophies. With our criteria in mind, we have selected 
the following projects: FindBugs, a bug-finding tool, Apache Ant, a build 
automation tool, and HSQLDB, a relational database management system. The 
statistics about these projects such as lines of code, Java file count, 
and revision number is shown in Table~\ref{tab:projects-eval}.


\begin{table}[t]
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline Project & \#{}SLOC & \#{}Java Files & Revision(SVN) \\
\hline FindBugs & 207324 & 1148 & r14310 \\
\hline Ant & 255441 & 1195 & r1301870 \\
\hline HSQLDB & 321 6 & 611 & r4960 \\
\hline
\end{tabular}
\end{center}
\caption{Selected open-source Java projects for evaluation}
\label{tab:projects-eval}
\end{table}


We ran our tool on each codebase and collected the output. Each run of 
our tool can find all four refactorings on a single codebase, but only 
for one threshold value for each refactoring. Therefore, we had to run 
our tool nine times to get all of the data. The data for each run 
consists of a file where each line is a refactoring suggestion pointing 
to a particular location in the codebase. The raw data is too large for 
us to present in this paper, so in Table~\ref{tab:data} we give the 
total number found for each type with a given threshold value. Each 
threshold value subsumes larger ones; suggestions listed in the output 
file for a threshold of three are all included in the output for a threshold 
of two.


\begin{table}[htb]
\begin{center}
\begin{tabular}{|c||c|c|c|}
\hline
\multicolumn{4}{|c|}{\textbf{Consolidate Conditional Expression (OR)}} \\
\hline Threshold & FindBugs & Ant & HSQLDB \\
\hline 2 & 160 & 38 & 99 \\
\hline 3 & 37 & 8 & 20 \\
\hline 4 & 11 & 1 & 5 \\
\hline
\multicolumn{4}{|c|}{\textbf{Consolidate Conditional Expression (AND)}} \\
\hline Threshold & FindBugs & Ant & HSQLDB \\
\hline 2 & 71 & 74 & 154 \\
\hline 3 & 1 & 0 & 6 \\
\hline 4 & 0 & 0 & 1 \\
\hline
\multicolumn{4}{|c|}{\textbf{Decompose Conditional}} \\
\hline Threshold & FindBugs & Ant & HSQLDB \\
\hline 2 & 411 & 229 & 169 \\
\hline 3 & 73 & 27 & 29 \\
\hline 4 & 25 & 9 & 7 \\
\hline
\multicolumn{4}{|c|}{\textbf{Introduce Explaining Variable}} \\
\hline Threshold & FindBugs & Ant & HSQLDB \\
\hline 20 & 236 & 63 & 75 \\
\hline 30 & 70 & 15 & 11 \\
\hline 40 & 33 & 7 & 1 \\
\hline
\multicolumn{4}{|c|}{\textbf{Consolidate Duplicate Conditional Fragments}} \\
\hline & FindBugs & Ant & HSQLDB \\
\hline & 15 & 21 & 21 \\
\hline
\end{tabular}
\end{center}
\caption{The number of suggestions different refactoring and thresholds.}
\label{tab:data}
\end{table}


We believe that these numbers, along with the precision and recall analysis 
in the next section demonstrate that our tool is useful. Even at higher 
(stricter) threshold values, there was never a case where our tool failed 
to find a violating instance. To get an idea about the kinds of instances our 
tool finds, we have extracted a few examples from some of the codebases for 
maximum threshold values.


Figure~\ref{fig:CCEOR-findbugs} shows an example of a consolidate 
conditional expression (\lstinline{OR}) refactoring found in the FindBugs 
codebase. There are four \lstinline{if} clauses which all have a single 
statement, \lstinline{return false;}. The conditional expressions 
can be merged using logical \lstinline{OR} to form a single condition, 
which can be further extracted as a method.


\begin{figure}[htb]
\begin{lstlisting}
if (!this.getMinPriority().equals(
    other.getMinPriority())) {
  return false;
}
if (this.getMinRank() != other.getMinRank()) {
  return false;
}
if (!this.hiddenBugCategorySet.equals(
    other.hiddenBugCategorySet)) {
  return false;
}
if (this.displayFalseWarnings != 
    other.displayFalseWarnings) {
  return false;
}
\end{lstlisting}
\caption{Consolidate conditional expression (OR) example from FindBugs}
\label{fig:CCEOR-findbugs}
\end{figure}


Our next refactoring example is a consolidate duplicate conditional 
fragments refactoring from the HSQLDB codebase. As shown in 
Figure~\ref{fig:CDCF-hsqldb}, the duplicated code fragment is 
\lstinline{patternIndex++;}, which occurs at the end of both the 
\lstinline{if} and \lstinline{else} branches. This statement can be 
refactored and pulled out of the two branches to just after the end 
of the \lstinline{if-else} block.


\begin{figure}[htb]
\begin{lstlisting}
if (source.charAt(sourceIndex) == 
    pattern.charAt(patternIndex)) {
  patternIndex++;
} else {
  final int tableValue = table[patternIndex];
  matchStart += (patternIndex - tableValue);
  if (patternIndex > 0) {
    patternIndex = tableValue;
  }
  patternIndex++;
}
\end{lstlisting}
\caption{Consolidate duplicate conditional fragment example from HSQLDB}
\label{fig:CDCF-hsqldb}
\end{figure}


The last example in Figure~\ref{fig:DC-ant} is a decompose 
conditional refactoring from the Apache Ant codebase. In the example the 
\lstinline{if} condition has six calls on an object identified by 
\lstinline{currentElement}. It looks like that there might be a better way 
for doing this comparison. One possibility is a method that compares 
\lstinline{currentElement} with other objects and returns true if it is 
equal to any of those objects. This method is a generalization of the 
current situation and can be used in other similar situations.


\begin{figure}[htb]
\begin{lstlisting}
if (currentElement.equals(HOME_INTERFACE)
           || currentElement.equals(REMOTE_INTERFACE)
           || currentElement.equals(LOCAL_INTERFACE)
           || currentElement.equals(LOCAL_HOME_INTERFACE)
           || currentElement.equals(BEAN_CLASS)
           || currentElement.equals(PK_CLASS)) {
           // Get the filename into a String object
           ...
\end{lstlisting}
\caption{Decompose conditional example from Apache Ant}
\label{fig:DC-ant}
\end{figure}


\subsection{Precision and Recall}
Measuring precision and recall reveals instances where our tool suggests code 
that is not refactorable or does not suggest code that is. Our methodology for 
finding precision and recall was to write tests fitting into one of three 
categories: true positive (TP), false positive (FP), and false negative (FN). 
Dividing TP by (TP + FP) yields precision. Similarly, diving TP by (TP + FN) 
yields recall. A test case approach was necessitated by the fact that we 
cannot find FN instances on real code manually beyond a small number of files. 
Additionally, we were able to think of many more corner cases that are 
troublesome for our tool that did not have specific instances in any of our 
codebases. Table~\ref{tab:eval-tests} shows the measurements for each of the 
test case categories.


\begin{table}[t]
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline Project & TP & FP & FN \\
\hline CCE (AND) & 6 & 0 & 2 \\
\hline CCE (OR) & 5 & 0 & 1 \\
\hline CDCF & 8 & 1 & 2 \\
\hline DC & 3 & 0 & 1 \\
\hline IEV & 5 & 4 & 1 \\
\hline
\end{tabular}
\end{center}
\caption{Test case categories}
\label{tab:eval-tests}
\end{table}


From these measurements, we can then derive a precision of 84.37\% and a 
recall of 79.41\%. We think these numbers show that a large number of 
suggestinons our tool found on the three codebases were refactorable, and 
that our tool did not miss many suggestions. Throughout the course of 
building and evaluating our tool, manual inspections we performed on each 
codease matches up with this conclusion.


\subsection{Performance}
Our last set of measurements is a runtime analysis that determines if our tool 
is fast enough for industrial use. For this analysis we have used the same  
three projects that we have already studied. The data is shown in 
Table~\ref{tab:proc-time}. Each number is the amount of seconds it took 
to parse through the entire codebase and output all suggestions for each 
refactoring pattern. The time was measured on a Linux machine, with 3GB of 
main memory running and an Intel(R) Core(TM)2 Duo CPU P8400 \@ 2.26GHz. 
The running time of different threshold values (not shown) is similar enough 
to be explained by variability due to operating system constraints, so we have 
only listed the running time measured with one set of threshold values.


\begin{table}[htb]
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline & FindBugs & Ant & HSQLDB \\
\hline Time(seconds): & 505.82 & 537.46 & 376.94 \\
\hline
\end{tabular}
\end{center}
\caption{Running time of our tool for each project}
\label{tab:proc-time}
\end{table}


\subsection{Conclusions}
From our data, we have gathered results that assess the viability of our 
tool. First and foremost, our tool was able to find a large number of 
suggestions for each codebase with few false positives or false 
negatives. The number of suggestions was also not too large 
because raising thresholds decreases the number of suggestions to only 
a dozen or so for each pattern. Our tool runs reasonably fast for an 
entire codebase, and near-instantly for single files. We have shown some 
examples of suggestions found by our tool for several refactoring types and 
argue that they are good candidates for refactoring. These examples 
were not cherry picked because we pulled them from runs using higher 
threshold values, which did not have many suggestions for each type. 


\subsection{Threats to Validity}
The first experiment we conducted does not represent a likely use-case of 
our tool in practice. Developers are unlikely to modify a working and tested 
code which is not being actively maintained. A more practical use case is 
running the tool automatically for a single file that is currently being 
worked on. Developers are more likely to be open for suggestions about code 
that they are in the process of writing, especially since the code has not 
been tested. Our Eclipse plugin facilitates this kind of work-in-progress 
use. As an additional benefit, running the tool on single files drastically 
reduces its running time.


Writing test cases to measure precision and recall may be problematic because 
we are able to tune the numbers by writing more true positive cases, 
among other things. However, we try to limit this by not duplicating similar 
test cases, and by trying to think of as many pattern-breaking examples as 
possible. Because of this, we think that precision and recall values may 
actually be better on real code because such corner cases may be rare in 
practice.


Our tool requires the user to specify threshold values for each 
refactoring type. There is no obvious way for the user to know what 
threshold value will produce a certain number of results, so it is not 
clear how the threshold concept is useful beyond filtering an arbitrary 
number of results. However, user can tweak the threshold values to 
get the desired level of suggestions. 


One last threat to validity is simply the small number of 
refactoring types our tool handles. For our tool to be truly usable, 
we would need to implement several more types of refactorings, and we 
are not sure how many we could add that are implementable using our 
current set of techniques. Unlike other tools, there is no option for 
users to define their own patterns, which would be a major selling point. 
We will address this issue in Section~\ref{sec:futurework}.


