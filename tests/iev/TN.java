package iev;

/**
 * IEV true negative tests.
 * 
 * @author QualityCan
 */
public class TN {
  public boolean b;
  public int i;
  public double d;
  
  /**
   * Low complexity.
   */
  public void test1() {
    if (b) {
      System.out.println("Hello.");
    }
  }

  /**
   * High text length.
   */
  public void test2() {
    if (javaMethodWithRidiculouslyLongName()
        && anotherJavaMethodWithRidiculouslyLongName()
        && yetAnotherJavaMethodWithRidiculouslyLongName()) {
      System.out.println("Hello.");
    }
  }

  private boolean javaMethodWithRidiculouslyLongName() {
    return true;
  }

  private boolean anotherJavaMethodWithRidiculouslyLongName() {
    return true;
  }

  private boolean yetAnotherJavaMethodWithRidiculouslyLongName() {
    return true;
  }
}
