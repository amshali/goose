package goose.refactorings.dc;

import goose.visitors.Context;

import java.util.HashMap;
import java.util.Map;

import javax.lang.model.element.Name;

import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.Tree.Kind;
import com.sun.source.util.TreeScanner;
import com.sun.tools.javac.tree.JCTree.JCFieldAccess;
import com.sun.tools.javac.tree.JCTree.JCIdent;

public class MethodCallCounter extends TreeScanner<Boolean, Context> {

  private Map<Name, Integer> methodCallCounterMap = new HashMap<Name, Integer>();

  private boolean inMethodCall = false;

  @Override
  public Boolean visitMethodInvocation(MethodInvocationTree node, Context p) {
    this.inMethodCall = true;
    Boolean ret = node.getMethodSelect().accept(this, p);
    this.inMethodCall = false;
    return ret;
  }

  @Override
  public Boolean visitMemberSelect(MemberSelectTree node, Context p) {
    if (inMethodCall) {
      JCFieldAccess fieldAccess = (JCFieldAccess) node;
      if (fieldAccess.selected.getKind() == Kind.IDENTIFIER) {
        JCIdent ident = (JCIdent) fieldAccess.selected;
        addToMap(ident.name);
      }
    }
    return true;
  }

  private void addToMap(Name s) {
    if (methodCallCounterMap.get(s) == null) {
      methodCallCounterMap.put(s, 0);
    }
    methodCallCounterMap.put(s, methodCallCounterMap.get(s) + 1);
  }

  public Map<Name, Integer> getMethodCallCounterMap() {
    return methodCallCounterMap;
  }

}
