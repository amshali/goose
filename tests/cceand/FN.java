package cceand;

public class FN {

  void test1() {
    boolean b1 = true, b2 = true, b3 = true;
    if (b1) {
      if (b2) {
        System.out.println("if if");
      }
    } else {
      if (b3) {
        System.out.println("else if");
      }
    }
  }

  void test2() {
    boolean b1 = true, b2 = true, b3 = true;
    if (b1) {
      if (b2) {
        System.out.println("if if 1");
      }
      if (b3) {
        System.out.println("if if 2");
      }
    }
  }

}
