
\documentclass{acm_proc_article-sp}

\begin{document}

\title{Refactoring Suggestion Tool for Java}

\numberofauthors{2}

\author{
\alignauthor
Amin Shali\\
       \affaddr{Computer Science Department}\\
       \affaddr{University of Texas at Austin}\\
       \email{amshali@cs.utexas.edu}
\alignauthor
Eric Huneke\\
       \affaddr{Electrical and Computer Eng. Department}\\
       \affaddr{University of Texas at Austin}\\
       \email{qualitycan@gmail.com}
}

\maketitle
\begin{abstract}
Refactoring is widely believed to be an effective technique to improve the 
quality of software code, especially in object-oriented systems. The first step 
in refactoring is to locate where it can be applied. In this paper, we propose 
various methods to identify locations of a number of refactoring opportunities 
in a codebase. We then propose a way to evaluate each method by consulting an 
expert and investigating whether the suggested opportunity has been refactored in 
a future revision of the code. 
\end{abstract}

%\category{D.2.3}{Software Engineering}{Coding Tools and Techniques -- Object Oriented Programming}
%
%\terms{Refactoring}

\keywords{Refactoring, Java, Automation, Code Smell} % NOT required for Proceedings

\section{Introduction}
Refactoring is a set of techniques and practices for altering the 
structure of a software program in order to improve its 
quality~\cite{Fowler1999}. In this context, quality translates 
to the amount of effort that is needed to maintain~\cite{Griswold:1992:PRA:144627} 
and change the code in the future. Software is not a static 
entity. It changes over time for different reasons such as adding 
new features and fixing bugs. Refactoring helps in reducing the 
complexity of the code and eliminating something that is called a 
\textit{code smell}~\cite{Fowler1999}. A code smell is a 
bad programming practice that can cause the code to be unreadable 
or prone to unexpected errors and bugs. 

The effect of refactoring on software quality has been studied in a number 
works~\cite{Alshayeb:2009:EIR:1558382.1558413,Soetens_Demeyer_2010,Moser_R_2008}.
Soetens et al. studies the effect of refactoring on PMD, a Java source code analyzer.
They do a mathematical assessment of the complexity and realize that while some 
refactorings do reduce the complexity of the code, some do not. Moser et al. also
reports some counterintuitive results on the effect of refactoring on 
quality and productivity of an agile team. Their conclusion is that the effect of 
refactoring is highly dependent on the context of study and could vary from 
one case to the other. 

Moreover, Alshayeb~\cite{Alshayeb:2009:EIR:1558382.1558413} 
reports that the effect of refactoring on adaptability, maintainability, 
understandability, reusability, and testability of software varies a lot and does 
not follow a special trend. All of the above findings, however, result from a special 
context and a limited number of case studies. There is no unanimous consensus 
on whether refactoring is useful or not. However, the common belief is 
that refactoring in general is useful.



The refactoring process has three parts: 
\begin{enumerate}
\item Identifying where to apply refactoring and what kind of refactoring to apply.
\item Applying the desired refactoring to the code.
\item Testing to make sure that the refactoring has not caused any problem. 
\end{enumerate}

Fowler~\cite{Fowler1999} demonstrates many refactoring patterns (over 60) in his 
book. A refactoring pattern is a description of a situation in the code 
where some improvements can be applied. For instance, there is a pattern 
called \textit{encapsulate field}. This pattern describes a situation 
where a field is public and it is better to change it to private 
and make it available through accessor methods. 

Once one identifies where to apply the refactoring and what kind of refactoring to 
apply, the process of changing the structure of the code is mostly mechanical. 
In fact, there are lots of tools and IDEs such as \textit{Eclipse}, 
\textit{IntelliJ IDEA}, and \textit{NetBeans} that will perform a
number of refactoring automatically. 

Despite all the work that has been done in this area, we think that there are 
still some opportunities left to exploit. In this work we are interested in the 
first part of the refactoring process; identifying the locations where one or 
more refactoring can be applied. 


\section{Proposal}
We propose a tool that identifies code smells in a codebase that are candidates 
for refactoring.  The tool will take as input a directory structure containing 
code and will output an ordered list of code snippets recommended for 
refactoring.  Because we believe that Java has the best balance between 
expressiveness and availability of code analysis tools, Java will be our target 
and implementation language.  Our first priority will be to implement the main 
features of the program.  This necessitates a back end that can analyze Java 
code and determine if the code needs to be refactored by a fixed set of rules.  
We will write a simple command-line interface to this back end.  If time permits, 
we may also build a GUI front end for ease-of-use.

The research in this area has shown that different refactoring opportunities are 
best detected by different means.  To ease our implementation efforts, we have 
selected a small set of refactorings that, by our analysis, can all be found by 
analyzing the code using \textit{abstract syntax trees} 
and the help of a \textit{data flow analyzer}.  Specifically, these refactorings are 
\textit{introduce explaining variable}, \textit{decompose conditional}, 
\textit{consolidate conditional expression} and \textit{duplicate conditional fragments}, 
\textit{introduce null object}, \textit{remove control flag}~\cite{sourcemaking}.  
In addition to their similarity, we have also selected these because we have 
not found a Java tool that automatically identifies these refactorings in the 
literature. 
PMD~\cite{PMD} is a Java source code analyzer which looks for buggy, dead, 
suboptimal, duplicate, and overcomplicated codes. We are going to borrow some ideas 
from the PMD project for implementing our tool. 



\section{Evaluation}
Our evaluation of the tool will be in the form of a case study investigating 
the performance of the tool on an open-source Java application.  We may select 
the project before our tool is actually complete so that we can leverage the 
project's history in order to help us build the tool.  There are several criteria 
for the project we use in the case study.  First, its complete version history must 
be readily accessible using a version control system.  In terms of size, the 
larger the better.  Our tool may be slow such that finding refactoring opportunities 
on the entire repository may be too time consuming.  In that case, we can simply 
run the tool on a subset of the project.  

The second criteria is likelihood of the 
existence of refactoring opportunities.  This is not directly measurable, but 
may be inferred from some other statistics.  Project length is a good indicator 
since it implies the addition of new features over time that were not envisioned 
during the original architecture.  Another indicator is number of distinct 
developers; more developers may increase the number of cause code smells due to 
increased inexperience or differing design philosophies.  All that being said, 
finding a suitable project will likely be an iterative process as there is 
nothing stopping us from trying out our tool on several different open-source 
projects.

When our tool is complete, our final study will consist of running the tool on 
some subset of the selected project's files at some old revision.  We will inspect 
the program's output and subjectively determine the quality of the resulting 
suggestions.  Additionally, we will attempt to determine how many of the suggested
refactorings eventually took place by looking at the latest revision of the code.  
To help with that task, we may write scripts to find files with check-in comments
indicating that refactoring changes were made.  Ideally, the evaluation process 
would be automated, but the main focus of our project will be to build the tool, 
so empirically investigating its effectiveness is slightly beyond the scope of 
our intended work.  However, we hope to get some promising results out of the 
manual study we intend to perform.

\section{Related Work}
In~\cite{Tsantalis_Chaikalis_Chatzigeorgiou_2008}, Tsantalis et al. 
demonstrates JDeoderant which is a tool for identifying opportunities 
for a number of refactorings in Java programs. In this paper, they identify 
the type-checking bad smell. Their tool also applies a refactoring 
algorithm to fix the issue. The two types of refactoring algorithms are 
\textit{replace conditional with polymorphism} and 
\textit{replace type code with state/strategy}. 
They evaluate their tool by running the tool on three examples 
from~\cite{OORP} and~\cite{Fowler1999}. 
Then, they check whether the tool correctly identifies the bad smells and 
fixes them. Our work is very similar to their work. However, we are aiming for 
different refactoring opportunities. In addition, our tool only suggest the 
locations where refactoring can be applied, but does not fix them.

Tsantalis et al.~\cite{Tsantalis_Chatzigeorgiou_2009} presents a method for 
identifying the location of \textit{extract method} refactorings and how to perform 
them. They use program slicing techniques for extracting the part 
of the method that can be separated into a new method. The part of the method 
that can be extracted is where some computation is done for computing a single 
variable. 

In addition, Tsantalis et al.~\cite{DBLP:journals/tse/TsantalisC09} 
presents a similar work on \textit{move method} refactorings. Their methodology 
for move method refactorings is to solve the \textit{feature envy} design problem. 
In their work they use a notion of \textit{distance} between a method and a class
in order to locate the feature envy bad smells. They use a metric 
based~\cite{Simon01metricsbased} method to suggest a refactoring opportunity. 
Based on the defined distance of a 
method from a set of classes, they devise an entity placement metric to move 
a method. This metric is based on low coupling and high cohesion principles. 
For evaluating their suggestion algorithm, they have asked a designer to give 
them feedback and suggestions. 
The theme of our work is similar to theirs. We also want to automatically identify 
the locations that a refactoring can be applied. However, we do not know 
if we are going to use the notion of distance they have used in our work. 
Our methodology could be very different from theirs based on the type of 
refactoring we are aiming for. The idea of asking an expert to provide feedback 
is interesting and we can do the same thing to evaluate our suggestion algorithm. 

Naouel Moha et al.~\cite{Moha08adomain} propose a domain 
specific language for defining code defects.
Using this language an expert can specify the properties of a code smell and 
their tool generates an algorithm for automatically identifying that defect.
The idea of having a language to specify the defects seems very interesting and
promising. Nonethless it has its own limitation. In their work they mention 
that they generated algorithm for detecting 15 different code smells. These code 
smells are either structural, lexical or measure based. The power of the language 
in specifying refactorings such as \textit{introduce null object} or 
\textit{consolidate duplicate conditional fragments} is a question for us. 
They specifically mention that they
are aiming for design defects, but they do not mention whether their language can 
model all the known design defects. Their approach to locate the design defects 
and code smells is different from ours in two dimensions. First, we are aiming 
for different set of refactoring opportunities. Second, we write the algorithm 
whereas they generate the algorithm.


Tourwe and Mens~\cite{Tourwe:2003:IRO:872754.873568}, propose a way to detect 
refactoring opportunities by using logic meta programming. Their method uses SOUL, 
a Prolog variant that can automatically access all program entities within the 
latest version of the target source code. To identify a particular refactoring, 
they represent the algorithm needed to find it as a succinct set of SOUL predicates, 
the execution of which can quickly find violating instances. They found their 
technique to be effective at finding refactoring opportunities, but the list 
of proposed suggestions was often quite large. The authors note that their technique 
may be best suited for some types of refactorings, but other techniques, such as 
metrics-based, may be optimal for others.

Simon et al. employ metrics and visualizations to aid in identifying certain 
kinds of code smells suitable for~\cite{Simon01metricsbased} refactoring. 
They define a distance measure that is able to identify program entities with 
low cohesion, which is a way of describing entities that are physically close 
but logically separate. For four different types of code smells, this distance 
metric is mapped to whatever entities make sense. Then, cases when each smell 
should be refactored are identified. Finally, a visualization of the source code 
is generated by running the distance metric on a particular smell, and by the 
second step refactorings can be suggested. Although promising, their method may 
not be suitable for certain kinds of refactorings, and is highly dependent on 
the visualization tool's performance and usability.


\bibliographystyle{unsrt}
\bibliography{refactoring}


\end{document}
