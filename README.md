Goose
=====

Java Refactoring Suggestion Tool with focus on Conditionals
-----------------------------------------------------------

### Authors
Amin Shali (amshali@gmail.com), Eric Huneke (qualitycan@gmail.com)

## Abstract
Refactoring is widely believed to be an effective technique for improving the 
quality of software, especially in object-oriented systems. The first step 
in refactoring is to locate where it can be applied. In this project, we present 
a tool that locates refactoring opportunities relating to conditionals in a 
codebase. 


### Tool

    java -jar release/goose-1.0.jar
    
With the following arguments:

Usage:

    usage: Goose
      -cceand <arg>   Consolidate Conditional Expression And
      -cceor <arg>    Consolidate Conditional Expression Or
      -cdcf           Consolidate Duplicate Conditional Fragments
      -dc <arg>       Decompose Conditional
      -file <file>    File to analyze
      -iev <arg>      Introduce Explaining Variable

Sample usage:

    java -jar release/goose-1.0.jar -cceand 2 -cceor 2 -cdcf -file Test.java

