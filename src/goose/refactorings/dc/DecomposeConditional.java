package goose.refactorings.dc;

import goose.suggestion.DCRefactoringSuggestion;
import goose.suggestion.RefactoringSuggestion;
import goose.visitors.Context;
import goose.visitors.RefactoringSuggestionVisitor;

import java.util.Map;

import javax.lang.model.element.Name;

import com.sun.source.tree.IfTree;

public class DecomposeConditional extends RefactoringSuggestionVisitor {

  private int MIN_CALLS = 2;

  public DecomposeConditional(int mIN_CALLS) {
    super();
    MIN_CALLS = mIN_CALLS;
  }

  @Override
  public Boolean visitIf(IfTree node, Context p) {
    MethodCallCounter mc = new MethodCallCounter();
    node.getCondition().accept(mc, p);
    Map<Name, Integer> m = mc.getMethodCallCounterMap();
    for (Name s : m.keySet()) {
      if (m.get(s) >= MIN_CALLS) {
        RefactoringSuggestion rs = new DCRefactoringSuggestion(p, node, node);
        suggestions.add(rs);
        break;
      }
    }
    return super.visitIf(node, p);
  }

}
