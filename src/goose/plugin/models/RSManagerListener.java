package goose.plugin.models;

public interface RSManagerListener {
  public void refactoringSuggestionsChanged(
      RSManagerEvent event);

}
