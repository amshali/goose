package goose.refactorings.cce;

import goose.visitors.Context;
import goose.visitors.DefaultTreeVisitor;

import java.util.List;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;

public class EndsWithReturnVisitor extends DefaultTreeVisitor {

  @Override
  protected Boolean defaultAction(Tree t, Context p) {
    return false;
  }

  @Override
  public Boolean visitBlock(BlockTree node, Context p) {
    List<? extends StatementTree> statms = node.getStatements();
    if (statms.size() > 0) {
      StatementTree lastStatementTree = statms.get(statms.size() - 1);
      return lastStatementTree.accept(this, p);
    }
    return false;
  }

  @Override
  public Boolean visitReturn(ReturnTree node, Context p) {
    return true;
  }

  @Override
  public Boolean visitThrow(ThrowTree node, Context p) {
    return true;
  }

}
