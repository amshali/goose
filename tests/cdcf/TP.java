package cdcf;

/**
 * CDCF true positive tests.
 * 
 * @author QualityCan
 */
public class TP {
  public boolean b;
  public int i;
  public double d;
  
  private double total;
  private double price;

  /**
   * Website example.
   */
  public void test1() {
    if (isSpecialDeal()) {
      total = price * 0.95;
      send();
    } else {
      total = price * 0.98;
      send();
    }
  }

  private boolean isSpecialDeal() {
    return true;
  }

  private void send() {
    ;
  }

  /**
   * BStatements only.
   */
  public void test2() {
    if (b) {
      System.out.println("B");
      System.out.println("M1");
    } else if (b) {
      System.out.println("B");
      System.out.println("M2");
    } else {
      System.out.println("B");
      System.out.println("M3");
    }
  }

  /*
   * EStatements only.
   */
  public void test3() {
    if (b) {
      System.out.println("M1");
      System.out.println("E");
    } else if (b) {
      System.out.println("M2");
      System.out.println("E");
    } else {
      System.out.println("M3");
      System.out.println("E");
    }
  }

  /**
   * BStatements and EStatements.
   */
  public void test4() {
    if (b) {
      System.out.println("B");
      System.out.println("M1");
      System.out.println("E");
    } else if (b) {
      System.out.println("B");
      System.out.println("M2");
      System.out.println("E");
    } else {
      System.out.println("B");
      System.out.println("M3");
      System.out.println("E");
    }
  }

  /**
   * Without braces.
   */
  public void test5() {
    if (b)
      System.out.println("B");
    else if (b)
      System.out.println("B");
    else
      System.out.println("B");
  }

  /**
   * Mixed braces.
   */
  public void test6() {
    if (b) {
      System.out.println("B");
    } else if (b)
      System.out.println("B");
    else {
      System.out.println("B");
    }
  }

  /**
   * Same-line statements.
   */
  public void test7() {
    if (b) {
      System.out.println("B");
      System.out.println("M1");
      System.out.println("E");
    } else if (b) {
      System.out.println("B");
      System.out.println("M2");
      System.out.println("E");
    } else {
      System.out.println("B");
      System.out.println("M3");
      System.out.println("E");
    }
  }

  /**
   * Nested.
   */
  public void test8() {
    if (b) {
      if (b) {
        System.out.println("B");
      } else {
        System.out.println("B");
      }
    } else {
      if (b) {
        System.out.println("B");
      } else {
        System.out.println("B");
      }
    }
  }
}
