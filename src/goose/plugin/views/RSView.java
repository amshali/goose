package goose.plugin.views;

import java.util.Comparator;

import goose.plugin.models.IRSItem;
import goose.plugin.models.RSManager;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.part.*;
import org.eclipse.jface.viewers.*;
import org.eclipse.jface.action.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.*;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.SWT;

/**
 * This sample class demonstrates how to plug-in a new workbench view. The view
 * shows data obtained from the model. The sample creates a dummy model on the
 * fly, but a real implementation would connect to the model available either in
 * this or another plug-in (e.g. the workspace). The view is connected to the
 * model using a content provider.
 * <p>
 * The view uses a label provider to define how model objects should be
 * presented in the view. Each view can present the same model objects using
 * different labels and icons, if needed. Alternatively, a single label provider
 * can be shared between views in order to ensure that objects of the same type
 * are presented in the same way everywhere.
 * <p>
 */

public class RSView extends ViewPart {

  /**
   * The ID of the view as specified by the extension.
   */
  public static final String ID = "goose.views.RefactoringSuggestionView";

  private TableViewer viewer;
  private TableColumn descriptionColumn;
  private TableColumn typeColumn;
  private TableColumn pathColumn;
  private TableColumn nameColumn;
  private TableColumn locationColumn;

  private Action doubleClickAction;
  private RSViewSorter sorter;

  /**
   * The constructor.
   */
  public RSView() {
  }

  /**
   * This is a callback that will allow us to create the viewer and initialize
   * it.
   */
  public void createPartControl(Composite parent) {
    viewer = new TableViewer(parent, SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI
        | SWT.FULL_SELECTION);
    final Table table = viewer.getTable();
    descriptionColumn = new TableColumn(table, SWT.LEFT);
    descriptionColumn.setText("Description");
    descriptionColumn.setWidth(300);
    pathColumn = new TableColumn(table, SWT.LEFT);
    pathColumn.setText("Path");
    pathColumn.setWidth(200);
    nameColumn = new TableColumn(table, SWT.LEFT);
    nameColumn.setText("Name");
    nameColumn.setWidth(200);
    locationColumn = new TableColumn(table, SWT.LEFT);
    locationColumn.setText("Location");
    locationColumn.setWidth(75);
    typeColumn = new TableColumn(table, SWT.LEFT);
    typeColumn.setText("Type");
    typeColumn.setWidth(100);
    table.setHeaderVisible(true);
    table.setLinesVisible(false);

    viewer.setContentProvider(new RSViewContentProvider());
    viewer.setLabelProvider(new RSViewLabelProvider());
    viewer.setInput(RSManager.getManager());
    createTableSorter();

    // Create the help context id for the viewer's control
    PlatformUI.getWorkbench().getHelpSystem()
        .setHelp(viewer.getControl(), "Goose.viewer");
    makeActions();
    hookContextMenu();
    hookDoubleClickAction();
  }

  private void createTableSorter() {
    Comparator<IRSItem> nameComparator = new Comparator<IRSItem>() {
      public int compare(IRSItem i1, IRSItem i2) {
        return i1.getResourceName().compareTo(i2.getResourceName());
      }
    };
    Comparator<IRSItem> locationComparator = new Comparator<IRSItem>() {
      public int compare(IRSItem i1, IRSItem i2) {
        return i1.getLocation().compareTo(i2.getLocation());
      }
    };
    Comparator<IRSItem> typeComparator = new Comparator<IRSItem>() {
      public int compare(IRSItem i1, IRSItem i2) {
        return i1.getType().compareTo(i2.getType());
      }
    };
    Comparator<IRSItem> descriptionComparator = new Comparator<IRSItem>() {
      public int compare(IRSItem i1, IRSItem i2) {
        return i1.getDescription().compareTo(i2.getDescription());
      }
    };
    Comparator<IRSItem> pathComparator = new Comparator<IRSItem>() {
      public int compare(IRSItem i1, IRSItem i2) {
        return i1.getResourcePath().compareTo(i2.getResourcePath());
      }
    };

    @SuppressWarnings("unchecked")
    Comparator<Object>[] comparators = new Comparator[] {
        descriptionComparator, pathComparator, nameComparator,
        locationComparator, typeComparator };
    sorter = new RSViewSorter(viewer, new TableColumn[] { descriptionColumn,
        pathColumn, nameColumn, locationColumn, typeColumn }, comparators);
    viewer.setSorter(sorter);
  }

  private void hookContextMenu() {
    MenuManager menuMgr = new MenuManager("#PopupMenu");
    menuMgr.setRemoveAllWhenShown(true);
    menuMgr.addMenuListener(new IMenuListener() {
      public void menuAboutToShow(IMenuManager manager) {
      }
    });
    Menu menu = menuMgr.createContextMenu(viewer.getControl());
    viewer.getControl().setMenu(menu);
    getSite().registerContextMenu(menuMgr, viewer);
  }

  private void makeActions() {
    doubleClickAction = new Action() {
      public void run() {
        ISelection selection = viewer.getSelection();
        IRSItem obj = (IRSItem) ((IStructuredSelection) selection).getFirstElement();
        EditorUtil.openEditor(getSite().getPage(),
            viewer.getSelection(), obj.getLocation());
      }
    };
  }

  private void hookDoubleClickAction() {
    viewer.addDoubleClickListener(new IDoubleClickListener() {
      public void doubleClick(DoubleClickEvent event) {
        doubleClickAction.run();
      }
    });
  }

  private void showMessage(String message) {
    MessageDialog.openInformation(viewer.getControl().getShell(),
        "Refactoring Suggestion View", message);
  }

  /**
   * Passing the focus request to the viewer's control.
   */
  public void setFocus() {
    viewer.getControl().setFocus();
  }
}