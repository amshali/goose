package goose.visitors;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.util.TreeScanner;

public class IfVisitor<R, P> extends TreeScanner<R, P> {
  
  @Override
  public R visitIf(IfTree node, P p) {
    StatementTree elseTree = node.getElseStatement();
    // If else's contents is an IfTree or a BlockTree containing solely an
    // IfTree, then accept this on it.
    if (elseTree instanceof IfTree) {
      ((IfTree)elseTree).accept(this, p);
    }
    else {
      if (elseTree instanceof BlockTree) {
        BlockTree blockTree = (BlockTree)elseTree;
        if (blockTree.getStatements().size() == 1
            && blockTree.getStatements().get(0) instanceof IfTree) {
          ((IfTree)blockTree.getStatements().get(0)).accept(this, p);
        }
      }
    }
    return null;
  }
}
