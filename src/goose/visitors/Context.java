package goose.visitors;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.Trees;

public class Context {
  public Trees trees;
  public SourcePositions sourcePositions;
  public CompilationUnitTree compilationUnitTree;
}
