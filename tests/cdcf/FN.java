package cdcf;

/**
 * CDCF false negative tests.
 * 
 * @author QualityCan
 */
public class FN {
  public boolean b;
  public int i;
  public double d;
  
  /**
   * Switch.
   */
  public void test1() {
    switch (i) {
    case 0:
      System.out.println("B");
      break;
    case 1:
      System.out.println("B");
      break;
    default:
      System.out.println("B");
      break;
    }
  }

  /**
   * Functionally-identical, but syntactically-different statements.
   */
  public void test2() {
    if (b) {
      System.out.print("B");
      System.out.println();
    } else {
      System.out.println("B");
    }
  }
}
