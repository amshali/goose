package iev;

/**
 * IEV false negative tests.
 * 
 * @author QualityCan
 */
public class FN {
  public boolean b;
  public int i;
  public double d;
  
  /**
   * Ternary operator.
   */
  public void test1() {
    boolean bool = (b || (new Integer(i)).toString().contains("Whatever")
        && new Double(d).compareTo(new Double(5.0)) > 55 || !(i >> 2 > 0)
        && (new String()) instanceof Object) ? true : false;
    System.out.println(bool);
  }
}
