package goose.refactorings.iev;

import goose.suggestion.IEVRefactoringSuggestion;
import goose.suggestion.RefactoringSuggestion;
import goose.visitors.Context;
import goose.visitors.RefactoringSuggestionVisitor;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.StatementTree;

public class IEVVisitor extends RefactoringSuggestionVisitor {

  private int COMPLEXITY_THRESHOLD = 30;

  public IEVVisitor(int cOMPLEXITY_THRESHOLD) {
    super();
    COMPLEXITY_THRESHOLD = cOMPLEXITY_THRESHOLD;
  }

  @Override
  public Boolean visitBlock(BlockTree node, Context p) {
    for (StatementTree st : node.getStatements()) {
      if (st instanceof IfTree) {
        IEVIfVisitor ifVisitor = new IEVIfVisitor();
        st.accept(ifVisitor, null);
        if (ifVisitor.complexity > COMPLEXITY_THRESHOLD) {
          RefactoringSuggestion rs = new IEVRefactoringSuggestion(p, st, st);
          suggestions.add(rs);
        }
      }
    }
    return super.visitBlock(node, p);
  }

}
