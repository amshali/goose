package goose.refactorings.cdcf;

import goose.suggestion.CDCFRefactoringSuggestion;
import goose.suggestion.RefactoringSuggestion;
import goose.visitors.Context;
import goose.visitors.IfVisitor;

import java.util.ArrayList;
import java.util.List;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.StatementTree;

public class CDCFIfVisitor extends IfVisitor<Void, Context> {
  
  public List<BlockTree> blocks = new ArrayList<BlockTree>();
  
  @Override
  public Void visitIf(IfTree node, Context p) {
    StatementTree thenStatement = node.getThenStatement();
    if (thenStatement instanceof BlockTree) {
      this.blocks.add((BlockTree)thenStatement);
    }
    else {
      this.blocks.clear();
      return null;
    }
    StatementTree elseStatement = node.getElseStatement();
    if (elseStatement instanceof IfTree) {
      ;
    }
    else if (elseStatement instanceof BlockTree) {
      this.blocks.add((BlockTree)elseStatement);
    }
    else {
      this.blocks.clear();
      return null;
    }
    super.visitIf(node, p);
    return null;
  }
  
  public RefactoringSuggestion analyze(StatementTree st, Context p) {
    
    if (blocks.size() <= 1) return null;
    
    List<StatementTree> bStats = new ArrayList<StatementTree>();
    List<StatementTree> eStats = new ArrayList<StatementTree>();
    
    int minSize = Integer.MAX_VALUE;
    for (BlockTree b: this.blocks) {
      minSize = Math.min(minSize, b.getStatements().size());
    }
    
    List<? extends StatementTree> firstList = 
      this.blocks.get(0).getStatements();
    StatementTree firstStat;
    List<? extends StatementTree> currList;
    StatementTree currStat;
    
    // Go from the end, filling in eStats.
    for (int x = 1; x <= minSize; x++) {
      firstStat = firstList.get(firstList.size() - x);
      boolean succeed = true;
      for (int y = 1; y < this.blocks.size(); y++) {
        currList = this.blocks.get(y).getStatements();
        currStat = currList.get(currList.size() - x);
        if (!firstStat.toString().equals(currStat.toString())) {
          succeed = false;
          break;
        }
      }
      if (succeed) eStats.add(firstList.get(firstList.size() - x));
      else break;
    }
    
    // Go from the beginning, filling in bStats.
    for (int x = 0; x < minSize; x++) {
      firstStat = firstList.get(x);
      boolean succeed = true;
      for (int y = 1; y < this.blocks.size(); y++) {
        currList = this.blocks.get(y).getStatements();
        currStat = currList.get(x);
        if (!firstStat.toString().equals(currStat.toString())) {
          succeed = false;
          break;
        }
      }
      if (succeed) bStats.add(firstList.get(x));
      else break;
    }
    
    if (bStats.size() != 0 || eStats.size() != 0) {
      return new CDCFRefactoringSuggestion(p, st, st);
    }
    
    return null;
  }
}
