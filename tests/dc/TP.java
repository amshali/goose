package dc;

import java.util.Date;

public class TP {
  private static final Date SUMMER_START = new Date(2012, 6, 20);
  private static final Date SUMMER_END = new Date(2012, 9, 20);

  public static void test1() {
    Date date = new Date();
    int charge = 0;
    int quantity = 100;
    int _winterRate = 10;
    int _winterServiceCharge = 4;
    int _summerRate = 8;
    if (date.before(SUMMER_START) || date.after(SUMMER_END))
      charge = quantity * _winterRate + _winterServiceCharge;
    else
      charge = quantity * _summerRate;
    
    System.out.println(charge);
  }

  public static void test2() {
    Date date = new Date();
    int charge = 0;
    int quantity = 100;
    int _winterRate = 10;
    int _winterServiceCharge = 4;
    int _summerRate = 8;
    if (!date.before(SUMMER_START) && date.after(SUMMER_END) || 
        date.after(SUMMER_END))
      charge = quantity * _winterRate + _winterServiceCharge;
    else
      charge = quantity * _summerRate;
    
    System.out.println(charge);
  }

  public static void test3() {
    Date date = new Date();
    int charge = 0;
    int quantity = 100;
    int _winterRate = 10;
    int _winterServiceCharge = 4;
    int _summerRate = 8;
    if (!date.before(SUMMER_START) && date.after(SUMMER_END) || 
        date.after(SUMMER_END))
      charge = quantity * _winterRate + _winterServiceCharge;
    else if (!date.before(SUMMER_START) && date.after(SUMMER_END))
      charge = quantity * _summerRate;
    
    System.out.println(charge);
  }
}
