package cceand;

public class TN {
  void test1() {
    boolean b1 = true, b2 = true;
    if (b1) {
      if (b2) {
        System.out.println("if if");
      }
    } else {
      System.out.println("else");
    }
  }

  void test2() {
    boolean b1 = true, b2 = true;
    if (b1) {
      if (b2) {
        System.out.println("if if");
      }
      System.out.println("in first if");
    } 
  }

  void test3() {
    boolean b1 = true, b2 = true;
    if (b1) {
      if (b2) {
        System.out.println("if if");
      }
      else {
        System.out.println("if if else");
      }
    } 
  }

}
