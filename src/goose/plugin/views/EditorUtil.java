package goose.plugin.views;

import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.texteditor.ITextEditor;

public class EditorUtil {
  public static void openEditor(IWorkbenchPage page, ISelection selection,
      int lineNumber) {
    // Get the first element.
    if (!(selection instanceof IStructuredSelection))
      return;
    Iterator<?> iter = ((IStructuredSelection) selection).iterator();
    if (!iter.hasNext())
      return;
    Object elem = iter.next();
    // Adapt the first element to a file.
    if (!(elem instanceof IAdaptable))
      return;
    IFile file = (IFile) ((IAdaptable) elem).getAdapter(IFile.class);
    if (file == null)
      return;
    // Open an editor on that file.
    try {
      ITextEditor editor = (ITextEditor) IDE.openEditor(page, file);
      IDocument document = editor.getDocumentProvider().getDocument(
          editor.getEditorInput());
      if (document != null) {
        IRegion lineInfo = null;
        try {
          // line count internaly starts with 0, and not with 1 like in
          // GUI
          lineInfo = document.getLineInformation(lineNumber - 1);
        } catch (BadLocationException e) {
          // ignored because line number may not really exist in document,
          // we guess this...
        }
        if (lineInfo != null) {
          editor.selectAndReveal(lineInfo.getOffset(), lineInfo.getLength());
        }
      }
    } catch (PartInitException e) {
      e.printStackTrace();
    }
  }

}
