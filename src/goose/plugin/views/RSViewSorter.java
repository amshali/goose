package goose.plugin.views;

import java.util.Comparator;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.TableColumn;

public class RSViewSorter extends ViewerSorter {
  // Simple data structure for grouping
  // sort information by column.
  private class SortInfo {
    int columnIndex;
    Comparator<Object> comparator;
    boolean descending;
  }
  
  
  private TableViewer viewer;
  private SortInfo[] infos;

  public RSViewSorter(TableViewer viewer, TableColumn[] columns,
      Comparator<Object>[] comparators) {
    this.viewer = viewer;
    infos = new SortInfo[columns.length];
    for (int i = 0; i < columns.length; i++) {
      infos[i] = new SortInfo();
      infos[i].columnIndex = i;
      infos[i].comparator = comparators[i];
      infos[i].descending = false;
      createSelectionListener(columns[i], infos[i]);
    }
  }

  public int compare(Viewer viewer, Object rs1, Object rs2) {
    for (int i = 0; i < infos.length; i++) {
      int result = infos[i].comparator.compare(rs1, rs2);
      if (result != 0) {
        if (infos[i].descending)
          return -result;
        return result;
      }
    }
    return 0;
  }

  private void createSelectionListener(final TableColumn column,
      final SortInfo info) {
    column.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent e) {
        sortUsing(info, column);        
      }
    });
  }

  
  protected void sortUsing(SortInfo info, final TableColumn column) {
    if (info == infos[0])
      info.descending = !info.descending;
    else {
      for (int i = 0; i < infos.length; i++) {
        if (info == infos[i]) {
          System.arraycopy(infos, 0, infos, 1, i);
          infos[0] = info;
          info.descending = false;
          break;
        }
      }
    }
    viewer.getTable().setSortColumn(column);
    viewer.getTable().setSortDirection(info.descending ? SWT.UP : SWT.DOWN);
    viewer.setSorter(this);
    viewer.refresh();
  }
}
