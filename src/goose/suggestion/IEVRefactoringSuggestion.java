package goose.suggestion;

import goose.visitors.Context;

import com.sun.source.tree.Tree;

public class IEVRefactoringSuggestion extends RefactoringSuggestion {

  public IEVRefactoringSuggestion(Context c, Tree start, Tree end) {
    super(c, start, end);
  }

  @Override
  public void setType() {
    type = RefactoringType.IntroduceExplainingVariable;
  }

}
