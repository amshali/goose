package dc;

import java.util.Date;

public class FN {
  private static final Date SUMMER_START = new Date(2012, 6, 20);
  private static final Date SUMMER_END = new Date(2012, 9, 20);
  private Date date = new Date();

  public int test1() {
    int charge = 0;
    int quantity = 100;
    int _winterRate = 10;
    int _winterServiceCharge = 4;
    int _summerRate = 8;
    if (date.before(SUMMER_START) || this.date.after(SUMMER_END))
      charge = quantity * _winterRate + _winterServiceCharge;
    else
      charge = quantity * _summerRate;
    
    System.out.println(charge);
    return charge;
  }

}
