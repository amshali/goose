package iev;

/**
 * IEV true positive tests.
 * 
 * @author QualityCan
 */
public class TP {
  public boolean b;
  public int i;
  public double d;
  
  private String platform;
  private String browser;
  private int resize;

  /**
   * Website example.
   */
  public void test1() {
    if ((platform.toUpperCase().indexOf("MAC") > -1)
        && (browser.toUpperCase().indexOf("IE") > -1) && wasInitialized()
        && resize > 0) {
      // do something
    }
  }

  private boolean wasInitialized() {
    return true;
  }

  /**
   * High complexity.
   */
  public void test2() {
    if (b || (new Integer(i)).toString().contains("Whatever")
        && new Double(d).compareTo(new Double(5.0)) > 55 || !(i >> 2 > 0)
        && (new String()) instanceof Object) {
      System.out.println("Hello.");
    }
  }

  /**
   * Second branch instead of first.
   */
  public void test3() {
    if (b) {
      System.out.println("Hello.");
    } else if (b || (new Integer(i)).toString().contains("Whatever")
        && new Double(d).compareTo(new Double(5.0)) > 55 || !(i >> 2 > 0)
        && (new String()) instanceof Object) {
      System.out.println("Hello.");
    } else {
      ;
    }
  }

  /**
   * Second branch with no braces.
   */
  public void test4() {
    if (b)
      ;
    else if (b || (new Integer(i)).toString().contains("Whatever")
        && new Double(d).compareTo(new Double(5.0)) > 55 || !(i >> 2 > 0)
        && (new String()) instanceof Object)
      ;
    else
      ;
  }

  /**
   * Nested inside another if.
   */
  public void test5() {
    if (b)
      ;
    else if (b) {
      if (b || (new Integer(i)).toString().contains("Whatever")
          && new Double(d).compareTo(new Double(5.0)) > 55 || !(i >> 2 > 0)
          && (new String()) instanceof Object)
        ;
    } else
      ;
  }
}
