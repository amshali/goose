package cceor;

public class TN {
  int test1() {
    boolean b1 = true, b2 = true, b3 = true;
    if (b1) {
      return 1;
    }
    if (b2 && b3) {
      return 2;
    }
    return 0;
  }
  
  void test2() {
    boolean b1 = true, b2 = true, b3 = true, b4 = true;
    if (b1) {
      System.out.println("test 1");
      return;
    }
    if (b2 && b3) {
      System.out.println("test 2");
      return;
    }
    if (b2 || b4) {
      System.out.println("test 3");
      return;
    }
  }
  
  void test3() throws Exception {
    boolean b1 = true, b2 = true, b3 = true, b4 = true;
    if (b1) {
      System.out.println("test");
      throw new Exception();
    }
    if (b2 && b3) {
      System.out.println("test");
      throw new NumberFormatException();
    }
    if (b2 || b4) {
      System.out.println("test");
      throw new Exception();
    }
  }
  
  Object test4() {
    boolean b1 = true, b2 = true, b3 = true, b4 = true;
    if (b1 || b4) {
      System.out.println("test");
      return new Object();
    }
    if (b2 && b3) {
      System.out.println("test");
      return new Integer(0);
    }
    return null;
  }
  

}
